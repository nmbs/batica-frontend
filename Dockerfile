 # Base nginx image
FROM nginx:1.15

# Install curl for healthcheck
RUN apt-get update && \
    apt-get install -y curl

# Metadata
ARG uid=1000
ARG gid=1000
ARG user=www-data
ARG group=www-data
ARG http_port=8080
ARG HEALTHCHECK_CMD="curl -f http://localhost:${http_port} || exit 1"

LABEL image.batica-frontend.maintainer=developer@cacd2.fr

# Custom nginx config
COPY ./config/nginx-custom.conf /etc/nginx/conf.d/default.conf

# Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*

# Copy application
COPY dist /usr/share/nginx/html/

# Application configuration file
COPY ./config/config.json /usr/share/nginx/html/assets/configuration/config.json

# Add entrypoint to rewrite configuration at launch time
COPY docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh

# Update rights so that nginx can be run as non-root user
RUN chown -R ${user}:${group} /usr/share/nginx/html/assets/configuration && \ 
  touch /var/run/nginx.pid && \
  chown -R ${user}:${group} /var/run/nginx.pid && \
  chown -R ${user}:${group} /var/cache/nginx && \
  chown -R ${user}:${group} /docker-entrypoint.sh

# Expose API port, can not export 80 as running as non-root
EXPOSE ${http_port}

# Run as user
USER ${user}

# Server healthcheck
HEALTHCHECK CMD ${HEALTHCHECK_CMD}

# Set entrypoint
ENTRYPOINT ["/docker-entrypoint.sh"]

# Run nginx as default command
CMD ["nginx", "-g", "daemon off;"]
