# BaticaFront

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.5.

## Installation

```bash

node -v
v9.4.0
npm -v
5.6.0

# Install
npm install

# Local test
http://localhost:4200

 ```

## Configuration
### Poste de développement
Sur le poste de développement, l'application utilise le fichier `src/assets/configuration/config.json` pour référencer les valeurs de configuration.

### CI QA & DEMO
Lors de la création du conteneur Docker, le fichier `config/config.json` est copié à la place du fichier de configuration de développement (`/usr/share/nginx/html/assets/configuration`). Les valeurs spécifiques aux environnements sont spécifiées sous forme de variables d'environnement, et sont résolues au démarage du conteneur (voir le fichier `docker-entrypoint`).
Dans le cadre du déploiement sur la plateforme AWS/kubernetes, les variables d'environnement sont valuées dans le projet **batica-solution**


## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-c [ci, qa, demo]` flag for a custom environment build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

