import {async, TestBed} from '@angular/core/testing';
import {SettingsComponent} from './settings.component';

describe('SettingsComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        SettingsComponent
      ],
    }).compileComponents();
  }));

});
