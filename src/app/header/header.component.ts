import {Component} from '@angular/core';
import {UserService} from '../user/user.service';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  pageTitle: string = 'Batica';
  public display: boolean = true;

  constructor(private userService: UserService) {
    this.display = userService.isAuthorize();
  }
}
