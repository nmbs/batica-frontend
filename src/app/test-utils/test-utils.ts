import {of} from 'rxjs';
import {convertToParamMap} from '@angular/router';
import {} from 'jasmine';

export class ActivatedRouteMock {
  public paramMap = of(convertToParamMap({
    searchTerm: '1',
  }));

  public data = of({
    'dasboardResolverService': {
      'headerEntreprise': {
        'titre': 'header',
        'type': 'Header',
        'values': {
          'isBlacklist': null,
          'raisonSociale': {'source': 'rncs', 'value': 'TELELANGUE '},
          'scoreGroupe': null,
          'waldec': null,
          'cibles': null,
          'siret': {'source': '', 'value': '41487265500058'},
          'isin': null,
          'cote_bourse': null,
          'eco_responsable': null
        }
      },
      'webScraping': null,
      'widgets': [{
        'titre': 'IdCompagny',
        'type': 'Identity',
        'values': {
          'enseigne': null,
          'formeJuridique': {'source': 'rncs', 'value': '0 - Société par actions simplifiée à associé unique'},
          'raisonSociale': {'source': 'rncs', 'value': 'TELELANGUE '},
          'codeNAF': {'source': 'insee', 'value': '8559A - Formation continue d\'adultes'},
          'adresse': {'source': 'insee', 'value': '9 RUE MAURICE GRANDCOING 94200 IVRY SUR SEINE'},
          'avocatIdentifie': null,
          'capitalSocial': null,
          'sigle': null
        }
      }, {'titre': 'KPI', 'type': 'KPI_C', 'values': null}, {'titre': 'KPI', 'type': 'KPI_S', 'values': null}, {
        'titre': 'KPI',
        'type': 'KPI_A',
        'values': null
      }, {'titre': 'KPI', 'type': 'KPI_B', 'values': null}, {'titre': 'KPI', 'type': 'KPI_K', 'values': null}, {
        'titre': 'Leaders',
        'type': 'Leaders',
        'values': {
          'dirigeants': {
            'source': 'RNCS',
            'value': [{
              'nom': 'HANNEL',
              'prenom': 'Wolfgang',
              'dateNaissance': '21/03/1954',
              'villeNaissance': ' WOLFENBUTTEL ',
              'paysNaissance': 'ALLEMAGNE',
              'fonction': 'Président',
              'codeFonction': ''
            }, {
              'nom': 'CHEZAUD',
              'prenom': 'Claude',
              'dateNaissance': '20/11/1949',
              'villeNaissance': '69383 Lyon 03(69)',
              'paysNaissance': 'FRANCE',
              'fonction': 'Commissaire aux comptes suppléant',
              'codeFonction': ''
            }, {
              'nom': 'WEINSTEIN',
              'prenom': 'Paul Harris',
              'dateNaissance': '06/03/1951',
              'villeNaissance': ' New York ',
              'paysNaissance': 'ETATS-UNIS D\'AMERIQUE',
              'fonction': 'Membre du comité de surveillance',
              'codeFonction': ''
            }]
          }
        }
      }, {
        'titre': 'Localisation',
        'type': 'Localisation',
        'values': {
          'website': {'source': 'GOOGLE', 'value': 'http://www.telelangue.com/'},
          'latitude': {'source': 'OSM', 'value': 48.8131354},
          'numeroTelephone': {'source': 'GOOGLE', 'value': '01 43 90 48 00'},
          'longitude': {'source': 'OSM', 'value': 2.393143}
        }
      }, {'titre': 'Comptes', 'type': 'Comptes', 'values': null}, {
        'titre': 'Evenements juridiques de l\'entreprise',
        'type': 'Evenement',
        'values': {
          'evenements': {
            'source': 'bodacc',
            'value': [{
              'source': 'bodacc',
              'date': 1513209600000,
              'libelle': 'Modification de représentant..',
              'url': 'http://www.bodacc.fr/annonce/detail-annonce/B/20170240/1366'
            }, {
              'source': 'bodacc',
              'date': 1484870400000,
              'libelle': 'Modification de représentant..',
              'url': 'http://www.bodacc.fr/annonce/detail-annonce/B/20170014/6112'
            }]
          }
        }
      }],
      'sources': [{
        'nomSource': 'insee',
        'dateMajSource': {'millisSinceEpoch': 1514505600000, 'daysSinceEpoch': 17529, 'year': 2017, 'month': 12, 'day': 29}
      }, {
        'nomSource': 'rncs',
        'dateMajSource': {'millisSinceEpoch': 1493942400000, 'daysSinceEpoch': 17291, 'year': 2017, 'month': 5, 'day': 5}
      }, {
        'nomSource': 'osm',
        'dateMajSource': {'millisSinceEpoch': 1534723200000, 'daysSinceEpoch': 17763, 'year': 2018, 'month': 8, 'day': 20}
      }, {
        'nomSource': 'places',
        'dateMajSource': {'millisSinceEpoch': 1534896000000, 'daysSinceEpoch': 17765, 'year': 2018, 'month': 8, 'day': 22}
      }]
    }
  });
}


export class TestUtils {

  public getKeycloakServiceSpy(): any {
    const keycloakServiceSpy = jasmine.createSpyObj('KeycloakService', ['getKeycloakInstance']);
    const profile = {firName: 'John', lastName: 'Doe', email: 'john.doe@cacd2.fr'};
    const realms = {roles: ['admin']};
    keycloakServiceSpy.getKeycloakInstance.and.returnValue({profile: profile, token: 'abc', realmAccess: realms});
    return keycloakServiceSpy;
  }

  public getMockDashboardKPI(): any {
    return {
      type: 'KPI_C',
      values: {
        devise: 'EUR',
        //  annee: '2018',
        //    chiffreAffaireNet: 150345,
        effectif: 11212,
        disponibilitesNettes: 1540003,
        totalActifImmobilisationsNet: 10546535,
        //  totalDettesBrutes: 657888,
        valeurAjoute: 66667,
        excedentBrutExploitation: 12878888,
        capaciteAutofinancement: 65733,
        rentabiliteNette: 4553,
        fondRoulementNetGlobal: 77773,
        besoinsFondsRoulement: 8777,
        tresorieNette: 12000000,
        rendemantCapitauxPropres: 88,
        rendementRessourcesDurablesNettes: 64
      }
    };
  }

  public getMockDashboardBilan(): any {
    return {
      type: ''

    };
  }

  public getMockActivatedRoute(): any {
    return new ActivatedRouteMock();
  }

}
