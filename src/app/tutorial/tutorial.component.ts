import {Component, EventEmitter, OnInit} from '@angular/core';
import {User} from '../user/user';
import {UserCustomizationService} from '../user/user-customization.service';
import {UserService} from '../user/user.service';
import {UserCustomization} from '../user/user-customization';
import {ICompany} from '../company/company';
import {FavorisPortefeuilleService} from '../favoris-portefeuille/favoris-portefeuille.service';
import {FavorisRequest} from '../favoris-portefeuille/favoris-request';
import {Favoris} from '../favoris-portefeuille/favoris';
import {PortefeuilleRequest} from '../favoris-portefeuille/portefeuille-request';
import {ActivatedRoute, Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {BreadcrumbService} from '../breadcrumb/breadcrumb.service';
import {Crumb} from '../breadcrumb/breadcrumb';
import {OnboardingService} from '../onboarding/onboarding.service';

@Component({
  selector: 'tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.scss']
})
export class TutorialComponent implements OnInit {

  pageTitle: string = 'Batica';
  user: User;
  isUserNew: boolean;
  userCusto: UserCustomization;
  list = [];
  hide: string = '';
  translation: boolean = false;
  public OnHideTuto = new EventEmitter<boolean>();
  loading = false;
  listFavoris: Array<Favoris> = [];
  listePortefeuille: Array<Favoris> = [];
  public isFirstVisitWithoutTutorial = new EventEmitter<boolean>(true);
  constructor(private route: ActivatedRoute,
              private userCustomizationService: UserCustomizationService,
              private userService: UserService,
              private favorisPortefeuilleService: FavorisPortefeuilleService,
              private router: Router,
              private breadcrumbService: BreadcrumbService,
              private onboardingService: OnboardingService) {

  }

  ngOnInit(): void {
    if (!this.userService.isAuthorize()) {
      this.router.navigate(['pending-creation']);
    }
    this.route.data
      .subscribe((data: { userResolver: UserCustomization }) => {
        this.list = data.userResolver.history;
        this.userService.userCustomization = data.userResolver;
        this.user = data.userResolver;
        if (data.userResolver.newUser) {
           this.onboardingService.launchOnboarding();
        }
      });
    this.router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });
    this.favorisPortefeuilleService.getFavoris(this.userService.userCustomization.guid).subscribe(favoris => {
      favoris.forEach(favori => {
        this.listFavoris.push(favori);
      });
    });

    this.favorisPortefeuilleService.getPortefeuille(this.userService.userCustomization.guid).subscribe(portefeuille => {
      portefeuille.forEach(company => {
        this.listePortefeuille.push(company);
      });
    });

    let crumb: Crumb = {
      path: '/',
      param: '',
      label: 'Accueil',
      url: '/'
    };

    this.breadcrumbService.refreshBreadcrumb(crumb);
  }


  fadeoutTuto() {
    this.user.tutorial = true;
    this.hide = 'fade-out-tutorial';
    this.translation = true;
    this.OnHideTuto.next(true);
    this.userCustomizationService.putUser(this.user).subscribe(data => {

    });
  }

  public test() {
    this.isFirstVisitWithoutTutorial.next(true);
  }

  checkRouterEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.loading = true;
    }

    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loading = false;
    }
  }

  addFavoris(entreprise: ICompany): void {

    let favorisRequest: FavorisRequest;
    favorisRequest = {
      'waldec': entreprise.waldec,
      'siret': entreprise.siret,
      'idUtilisateur': this.userService.userCustomization.guid
    };


    this.favorisPortefeuilleService.postFavoris(favorisRequest).subscribe(data => {
      this.listFavoris.push(data);
      this.refreshListFavoris();
    });

  }

  addPortefeuilles(entreprise: ICompany): void {

    let portefeuilleRequest: PortefeuilleRequest;
    portefeuilleRequest = {
      'waldec': entreprise.waldec,
      'siret': entreprise.siret,
      'idUtilisateur': this.userService.userCustomization.guid
    };


    this.favorisPortefeuilleService.postPortefeuille(portefeuilleRequest).subscribe(data => {
      this.listePortefeuille.push(data);
      this.refreshListFavoris();
    });

  }

  refreshListFavoris(): void {
    this.favorisPortefeuilleService.getFavoris(this.userCusto.guid).subscribe(data => {
      data.forEach(element => {
        this.listFavoris.push(element);
      });
    });
  }

}
