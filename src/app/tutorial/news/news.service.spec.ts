import {inject, TestBed} from '@angular/core/testing';
import {NewsService} from '../../_shared/news.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {MockAppConfig} from '../../app.config.mock';
import {AppConfig} from '../../app.config';
import {HttpClient} from '@angular/common/http';

describe('NewsService', () => {
  beforeEach(() => {
    const appConfigSpy = new MockAppConfig();
    TestBed.configureTestingModule({
      providers: [NewsService, {
        provide: AppConfig,
        useValue: appConfigSpy
      }],
      imports: [HttpClientTestingModule]
    });

  });

  it('should be created', inject([NewsService], (service: NewsService) => {
    expect(service).toBeTruthy();
  }));
});
