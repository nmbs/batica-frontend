import {Component, OnInit} from '@angular/core';
import {Favoris} from '../../favoris-portefeuille/favoris';
import {Portefeuille} from '../../favoris-portefeuille/portefeuille';
import {NewsService} from '../../_shared/news.service';
import {CompanyService} from '../../company/company.service';
import {IArticle} from '../../_shared/news';
import {FavorisPortefeuilleService} from '../../favoris-portefeuille/favoris-portefeuille.service';
import {UserService} from '../../user/user.service';
import {forkJoin} from 'rxjs';

@Component({
  selector: 'news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsComponent implements OnInit {

  listFavoris: Array<Favoris> = [];
  listePortefeuille: Array<Portefeuille> = [];
  news: Array<IArticle>;

  constructor(private newsService: NewsService,
              private companyService: CompanyService,
              private favorisPortefeuilleService: FavorisPortefeuilleService,
              private userService: UserService) {
  }

  ngOnInit() {

    let tabRequest: Array<string> = [];

    let mesFavoris = this.favorisPortefeuilleService.getFavoris(this.userService.userCustomization.guid);
    let mesPortefeuilles = this.favorisPortefeuilleService.getPortefeuille(this.userService.userCustomization.guid);

    forkJoin([mesFavoris, mesPortefeuilles]).subscribe(results => {

      let favoris = results[0];
      let portefeuilles = results[1];

      this.companyService.getCompaniesForNews(favoris, portefeuilles).subscribe(companies => {
        companies.forEach(element => {
          tabRequest.push('"' + element.raisonSociale + '"');
        });
        if(tabRequest && tabRequest.length > 0) {
          this.newsService.getNews(tabRequest).subscribe(news => {
            news.forEach(article => {
              article.raisonSociale = article.raisonSociale.slice(1, article.raisonSociale.length - 1);
            });
            this.news = news.slice(0, 6);
          },
          (data: any) => {
            console.log("data error news :", data);
          });
        }
        else {
          this.news = [];
        }
      });
    });
  }

  calculTime(article: IArticle): any {

    let date = new Date(article.publishedAt);

    console.log("ouais la date", (Date.now() - date.getTime()) / 3600000);

    if (((Date.now() - date.getTime()) / 3600000) < 24) {
      console.log("bah on est dans le if");
      return Date.now() - date.getTime();
    }

    return date.getTime();
  }
}
