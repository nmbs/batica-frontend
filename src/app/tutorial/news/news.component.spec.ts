import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewsComponent} from './news.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppConfig} from '../../app.config';
import {MockAppConfig} from '../../app.config.mock';
import {CompanyService} from '../../company/company.service';
import {UserService} from '../../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {TestUtils} from '../../test-utils/test-utils';

describe('NewsComponent', () => {
  let component: NewsComponent;
  let fixture: ComponentFixture<NewsComponent>;

  beforeEach(async(() => {
    const mockAppConfig = new MockAppConfig();
    TestBed.configureTestingModule({
      declarations: [NewsComponent],
      providers: [{
        provide: AppConfig,
        useValue: mockAppConfig
      }, CompanyService, UserService, {provide: KeycloakService, useValue: new TestUtils().getKeycloakServiceSpy()}],
      imports: [HttpClientTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
