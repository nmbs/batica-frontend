import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';

import {MatPaginator, MatTableDataSource} from '@angular/material';
import {forkJoin} from 'rxjs';
import {UserCustomizationService} from '../../user/user-customization.service';
import {FavorisPortefeuilleService} from '../../favoris-portefeuille/favoris-portefeuille.service';
import {PortefeuilleRequest} from '../../favoris-portefeuille/portefeuille-request';
import {FavorisRequest} from '../../favoris-portefeuille/favoris-request';
import {ICompany} from '../../company/company';
import {UserService} from '../../user/user.service';
import {DashboardService} from '../../dashboard/dashboard.service';

@Component({
  selector: 'search-history',
  templateUrl: './search-history.component.html',
  styleUrls: ['./search-history.component.scss']
})
export class SearchHistoryComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('divAjoutPortefeuille', {read: ElementRef}) divAjoutPortefeuille: ElementRef;
  @ViewChild('divPortefeuille', {read: ElementRef}) divPortefeuille: ElementRef;
  @ViewChild('divFavori', {read: ElementRef}) divFavori: ElementRef;
  private entreprise: ICompany;
  divTroisPetitsPointsClicked;
  isCurrentEntreprisePresentInPortefeuille;
  isCurrentEntreprisePresentInFavori;
  errorMessage: string;
  searchTerm: string;
  isTableResult: boolean = false;
  dataSource = new MatTableDataSource([]);
  index = 0;
  pageNumber = 0;
  numbers = [];
  isDropDown: boolean = true;
  @Input() list;
  private sub: any;

  displayedColumns = ['raisonSociale', 'adresse', 'dirigeant', 'siege', 'status', 'info-complementaires', 'portefeuille'];
  rotate: string = 'icone-dropdown-click';
  ngStyle: Object = {};
  ngStyleDivTroisPetitsPoints: Object = {};

  favoris: any;
  portefeuilles: any;

  tousLesSiretsFavoris: Map<string, string> = new Map<string, string>();
  tousLesSiretsClients: Map<string, string> = new Map<string, string>();

  tousLesFavoris: any;
  tousLesClients: any;

  constructor(private route: ActivatedRoute,
              private userCustomizationService: UserCustomizationService,
              private userService: UserService,
              private dashboardService: DashboardService,
              private favorisPortefeuilleService: FavorisPortefeuilleService,
              private router: Router) {


  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {

      let mesFavoris = this.favorisPortefeuilleService.getFavoris(this.userService.userCustomization.guid);
      let mesPortefeuilles = this.favorisPortefeuilleService.getPortefeuille(this.userService.userCustomization.guid);

      forkJoin([mesFavoris, mesPortefeuilles]).subscribe(results => {

        this.favoris = results[0];

        for (var i = 0; i < this.favoris.length; i++) {
          if (this.favoris[i].siret) {
            this.tousLesSiretsFavoris.set(this.favoris[i].siret.replace(/ /g, ''), 'siret');
          }
          else {
            this.tousLesSiretsFavoris.set(this.favoris[i].waldec.replace(/ /g, ''), 'waldec');
          }
        }

        this.portefeuilles = results[1];

        for (var i = 0; i < this.portefeuilles.length; i++) {
          if (this.portefeuilles[i].siret) {
            this.tousLesSiretsClients.set(this.portefeuilles[i].siret.replace(/ /g, ''), 'siret');
          }
          else {
            this.tousLesSiretsClients.set(this.portefeuilles[i].waldec.replace(/ /g, ''), 'waldec');
          }
        }

        this.dashboardService.getCompaniesForFavPorte(this.tousLesSiretsClients).subscribe(data => {
          this.tousLesClients = data.slice(0, 6);
          this.tousLesClients.forEach(clienthaut => {
            this.portefeuilles.forEach(clientbas => {
              clienthaut.date = clientbas.date;
            });
          });
        });

        this.dashboardService.getCompaniesForFavPorte(this.tousLesSiretsFavoris).subscribe(data => {
          this.tousLesFavoris = data;
          this.tousLesFavoris.forEach(clienthaut => {
            this.favoris.forEach(clientbas => {
              clienthaut.date = clientbas.date;
            });
          });
        });

        this.list.forEach(entreprise => {

          let favoriFound = false;
          this.favoris.forEach(favori => {
            if ((entreprise.siret && entreprise.siret != '' && entreprise.siret == favori.siret) || (entreprise.waldec && entreprise.waldec != '' && entreprise.waldec == favori.waldec)) {
              entreprise.isFavori = favori.guid;
              favoriFound = true;
            }
          });
          if (!favoriFound) {
            entreprise.isFavori = null;
          }
          let portefeuilleFound = false;
          this.portefeuilles.forEach(portefeuille => {


            if ((entreprise.siret && entreprise.siret != '' && entreprise.siret == portefeuille.siret) || (entreprise.waldec && entreprise.waldec != '' && entreprise.waldec == portefeuille.waldec)) {
              portefeuille.raisonSociale = entreprise.raisonSociale;
              entreprise.isPortefeuille = portefeuille.guid;
              portefeuilleFound = true;
            }
          });
          if (!portefeuilleFound) {
            entreprise.isPortefeuille = null;
          }

        });
        this.dataSource = new MatTableDataSource(this.list);
      });

    });

  }

  allerPageEntreprise(client: any) {
    this.router.navigate(['entreprises', client.identifiant]);
  }

  search(entreprise: ICompany): void {
    this.router.navigate(['entreprises', entreprise.waldec ? entreprise.waldec : entreprise.siret]);
  }

  showDropDown(): void {
    this.isDropDown = !this.isDropDown;

    if (this.isDropDown) {
      this.rotate = 'icone-dropdown-click';
    }

    else {
      this.rotate = 'icone-dropdown-click-2';
    }

  }

  clickTroisPetitsPoints(event, item) {
    console.log(event);
    var style: string = 'position : absolute; background-size : contain; background-repeat : no-repeat; background-image: url(\'../../assets/images/bulle-favoris.svg\'); display : block; left : ' + String(event.clientX - 190) + 'px; top : ' + String(event.clientY) + 'px; width : 253px; height : 90px; z-index : 3;';
    this.divAjoutPortefeuille.nativeElement.style = style;
    console.log(style);

  }


  hideDivFavoris(event) {
    if (!event.target.attributes || !event.target.attributes['class'] || event.target.attributes['class'].nodeValue.indexOf('cdk-column-portefeuille mat-column-portefeuille') == -1 && event.target.attributes['class'].nodeValue.indexOf('picto-ajout') == -1) {

      this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille-gris.svg\')';
      this.entreprise.isSelected = false;
      this.ngStyle['display'] = 'none';
      if (this.divTroisPetitsPointsClicked) {
        this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille-gris.svg\')';
      }
    }
  }

  onClickTroisPetitsPoints(event, item) {

    var ngIfAfficherDiv = false;
    if (this.entreprise) {
      this.entreprise.isSelected = false;
    }

    var left = String(event.clientX - 220);
    var top = String(event.clientY + document.body.scrollTop + document.documentElement.scrollTop - 350);

    this.ngStyle = {'left': left + 'px', 'top': top + 'px', 'display': 'flex'};

    if (this.divTroisPetitsPointsClicked) {
      this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille-gris.svg\')';
    }

    this.entreprise = item;
    this.entreprise.isSelected = true;
    if (this.entreprise.isFavori != undefined) {
      this.isCurrentEntreprisePresentInFavori = true;
    }
    else {
      this.isCurrentEntreprisePresentInFavori = false;
    }
    if (this.entreprise.isPortefeuille != undefined) {
      this.isCurrentEntreprisePresentInPortefeuille = true;
    }
    else {
      this.isCurrentEntreprisePresentInPortefeuille = false;
    }
    if (!event.originalTarget)
      return;
    if (event.originalTarget.firstChild != null) {
      this.divTroisPetitsPointsClicked = event.originalTarget.firstChild;
    }
    else {
      this.divTroisPetitsPointsClicked = event.originalTarget;
    }
    this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille.svg\')';
  }


  addFavoris(): void {

    let favorisRequest: FavorisRequest;
    favorisRequest = {
      'waldec': this.entreprise.waldec == undefined || this.entreprise.waldec == null ? '' : this.entreprise.waldec,
      'siret': this.entreprise.siret == undefined || this.entreprise.siret == null ? '' : this.entreprise.siret,
      'idUtilisateur': this.userService.userCustomization.guid
    };
    if (this.entreprise.isFavori == undefined) {
      this.favorisPortefeuilleService.postFavoris(favorisRequest).subscribe(data => {
        this.list.map((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == favorisRequest.siret) || entreprise.waldec && (entreprise.waldec == favorisRequest.waldec)) {
            entreprise.isFavori = data.guid;
            this.list[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.list);
        });
      });
    }
    else {
      this.favorisPortefeuilleService.deleteFavori(this.entreprise.isFavori).subscribe(data => {
        this.list.map((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == favorisRequest.siret) || entreprise.waldec && (entreprise.waldec == favorisRequest.waldec)) {
            entreprise.isFavori = undefined;
            this.list[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.list);
        });

      });
    }
  }

  addPortefeuilles(): void {
    let portefeuilleRequest: PortefeuilleRequest;
    portefeuilleRequest = {
      'waldec': this.entreprise.waldec,
      'siret': this.entreprise.siret,
      'idUtilisateur': this.userService.userCustomization.guid
    };
    if (this.entreprise.isPortefeuille == undefined) {
      this.favorisPortefeuilleService.postPortefeuille(portefeuilleRequest).subscribe(data => {
        this.list.map((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == portefeuilleRequest.siret) || entreprise.waldec && (entreprise.waldec == portefeuilleRequest.waldec)) {
            entreprise.isPortefeuille = data.guid;
            this.list[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.list);
        });
      });
    }
    else {
      this.favorisPortefeuilleService.deletePortefeuille(this.entreprise.isPortefeuille).subscribe(data => {
        this.list.map((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == portefeuilleRequest.siret) || entreprise.waldec && (entreprise.waldec == portefeuilleRequest.waldec)) {

            entreprise.isPortefeuille = undefined;
            this.list[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.list);
        });
      });
    }

  }

}
