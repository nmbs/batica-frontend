import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {SearchHistoryComponent} from './search-history.component';
import {Observable, of} from 'rxjs';

import {FormsModule} from '@angular/forms';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';
import {ICompany} from '../../company/company';
import {CompanyService} from '../../company/company.service';

export class ActivatedRouteMock {
  public paramMap = of(convertToParamMap({
    searchTerm: '1',
  }));
}

class MockCompanyService {
  getCompaniesBySiren(searchTerm: string): Observable<ICompany[]> {

    return Observable.create((observer) => {
      const resp = [{
        'siret': '12345678910',
        'waldec': 'lenumerodewaldec1',
        'raisonSociale': 'Renault',
        'dirigeant': 'pierre jean',
        'adresse': {
          'voie': '16 rue dunois',
          'codePostal': '75006',
          'commune': 'paris'
        },
        'eirl': true,
        'activiteAuxiliaire': false,
        'siege': true,
        'status': 'KO'
      }];
      observer.next(resp);
      observer.complete();
    });
  }
};

describe('ListComponent', () => {
  let component: SearchHistoryComponent;
  let fixture: ComponentFixture<SearchHistoryComponent>;
  let activatedRoute: ActivatedRouteMock;
  let routerStub;
  beforeEach(async(() => {
    routerStub = {
      navigate: jasmine.createSpy('navigate')
    };
    activatedRoute = new ActivatedRouteMock();
    // activatedRoute.testParamMap = { searchTerm: '1' };
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule],
      providers: [
        {provide: CompanyService, useClass: MockCompanyService},
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      declarations: [SearchHistoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
