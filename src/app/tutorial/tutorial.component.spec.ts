import {async, TestBed} from '@angular/core/testing';
import {TutorialComponent} from './tutorial.component';

describe('TutorialComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TutorialComponent
      ],
    }).compileComponents();
  }));

});
