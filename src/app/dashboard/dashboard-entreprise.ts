import {Header} from './widgets/IHeader';

export interface IDashboardEntreprise {
  headerEntreprise: Header,
  webScraping: any,
  widgets: [
    {
      titre: string,
      type: string,
      values: any
    }
    ],
  sources: [
    {
      nomSource: string,
      dateMajSource: {
        day: string,
        month: string,
        year: string,
        daysSinceEpoch: string,
        millisSinceEpoch: string
      }
    }
    ]
}
