import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {IDashboardEntreprise} from './dashboard-entreprise';
import {FavorisPortefeuilleService} from '../favoris-portefeuille/favoris-portefeuille.service';
import {FavorisRequest} from '../favoris-portefeuille/favoris-request';
import {UserCustomizationService} from '../user/user-customization.service';
import {PortefeuilleRequest} from '../favoris-portefeuille/portefeuille-request';
import {Favoris} from '../favoris-portefeuille/favoris';
import {Portefeuille} from '../favoris-portefeuille/portefeuille';
import {UserService} from '../user/user.service';
import {forkJoin, timer} from 'rxjs';
import {IIdentity} from './widgets/IIdentity';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {Crumb} from '../breadcrumb/breadcrumb';
import {BreadcrumbService} from '../breadcrumb/breadcrumb.service';
import {DashboardService} from './dashboard.service';
import {IBilan} from './widgets/IBilan';
import {ILocalisation} from './widgets/ILocalisation';
import {IArticle} from '../_shared/news';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { windowToggle } from '../../../node_modules/rxjs/operators';

@Component({
  selector: 'ngbd-modal-content',
  template: `
    <div class="modal-header title-modal-surcharge">
      <h4 class="modal-title items_head_block">{{nomEntreprise}}</h4>
      <button type="button" class="close" aria-label="Close" (click)="activeModal.dismiss('Cross click')">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <p class="data_1">{{setTitle()}}</p>
    <div class="modal-body body-modal-surcharge">
      <div class="appetence" *ngFor="let cible of dashboard.headerEntreprise.values.cibles.value;let i = index">
        <p *ngIf="cible=='KCONFCCO'" class="data_2">Client Groupe Crédit Agricole  avec des encours Affacturage à la concurrence pouvant être repris par Crédit Agricole Leasing & Factoring… n’oubliez pas d’en parler à votre Ingénieur Commercial Factoring du Crédit Agricole Leasing & Factoring !</p>
        <p *ngIf="cible=='KCONFCCT'" class="data_2">Client Groupe Crédit Agricole avec des encours Court Terme à la concurrence pouvant être transformés en Affacturage… Pensez à lui présenter Cash In Time !</p>
        <p *ngIf="cible=='KCONFPRO'" class="data_2">Prospect éligible à de l’Affacturage… Pensez à lui présenter l’offre Cash In Time ou à le démarcher avec votre Ingénieur Commercial Factoring du Crédit Agricole Leasing & Factoring !</p>
        <p *ngIf="cible=='KCONFCAE'" class="data_2">Entreprise avec une activité Export éligible à de l’Affacturage pour l’accompagner dans sa relance clients à l’internationale… n’oubliez pas d’en parler à votre Ingénieur Commercial Factoring du Crédit Agricole Leasing & Factoring !</p>
        <p *ngIf="cible=='KCONFCCD'" class="data_2">Entreprise suspectée d’avoir de l’Affacturage à la concurrence … Pensez à lui présenter l’offre Cash In Time !</p>
        
        <p *ngIf="cible=='KCONCCCO'" class="data_2">Client Groupe Crédit Agricole ayant du Crédit-Bail Mobilier à la concurrence et familiarisé au Crédit-Bail Mobilier… Pensez à financer ses prochains projets en Crédit-Bail Mobilier avec  Greenlease !</p>
        <p *ngIf="cible=='KCONCPRO'" class="data_2">Prospect éligible à du Crédit-Bail Mobilier… Pensez à lui présenter les offres de Crédit-Bail Mobilier de Greenlease lorsque vous allez le démarcher ou en parler à votre Ingénieur Commercial Leasing du Crédit Agricole Leasing & Factoring !</p>
       
      </div>
    </div>
  `,
  styles: [
    " .modal-content {margin-top:50%; } .data_1 { font-family: Roboto, sans-serif;; font-size: 16px; font-weight: 100; font-style: normal; font-stretch: normal; line-height: normal; letter-spacing: 0.1px; color: #4a4a4a; margin-left: 3.2%; } .data_2 { font-size: 15px; color: #686868; } .items_head_block { color: #000000; font-weight: bold; font-size: 18px; padding-left: 0; } .title-modal-surcharge { border-bottom: none; padding-bottom: 0%; color: #009F61} .body-modal-surcharge { padding-top: 0%; } "
  ]
})
export class NgbdModalContent {

  @Input()
  nomEntreprise: string;

  @Input()
  dashboard: IDashboardEntreprise;

  constructor(public activeModal: NgbActiveModal) {

  }

  setTitle(): string {

    console.log("length cibles", this.dashboard.headerEntreprise.values.cibles.value.length)

    if(this.dashboard.headerEntreprise.values.cibles.value.length == 2) {
      return "Entreprise éligible à de l’Affacturage et du Crédit-Bail Mobilier";
    }
    else {
      let cible = this.dashboard.headerEntreprise.values.cibles.value[0];
      console.log("tototo", cible)
      if(cible=='KCONFCCO' || cible=='KCONFCCT' || cible=='KCONFPRO' || cible=='KCONFCAE' || cible=='KCONFCCD') {
        return "Entreprise éligible à de l’Affacturage";
      }
      else if(cible=='KCONCCCO' || cible=='KCONCPRO') {
        return "Entreprise éligible à du Crédit-Bail Mobilier";
      }

    }

  }
}


@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [NgbCarouselConfig]
})
export class DashboardComponent implements OnInit {
  POLLING_DELAY = 1000;
  MAX_NB_RETRY = 20;
  dashboard: IDashboardEntreprise;
  dashboardIdentityEntreprise: IIdentity;
  dashboardDirigeants: any;
  dashboardKPI: any;
  dashboardEvt: any;
  dashboardBilan: IBilan;
  dashboardLocalisation: ILocalisation;
  showLocalisationWidget: boolean;
  mesFavoris: Array<Favoris>;
  myFavori: Favoris;
  mesPortefeuilles: Array<Portefeuille>;
  myPortefeuille: Portefeuille;
  isFavori = false;
  isPortefeuille = false;
  raisonSociale: string;
  webscraping: boolean=false;
  devise: String;
  queryRaisonSociale: Array<string> = [];
  articles: IArticle[];
  styleTitle: string = "";
  isAppetence: boolean = false;


  constructor(private route: ActivatedRoute,  private config: NgbCarouselConfig,
    private favorisPortefeuilleService: FavorisPortefeuilleService,
    private userService: UserService,
    private userCustomizationService: UserCustomizationService,
    private dashboardService: DashboardService,
    private breadcrumbService: BreadcrumbService,
    private modalService: NgbModal) {
      console.log("constructor Dasboard Component");
   }
  counter = 0;
  resultatAvocat: any;
  avocatAlert(): boolean {
    return this.dashboardIdentityEntreprise.values.avocatIdentifie &&
    this.dashboardIdentityEntreprise.values.avocatIdentifie.value === 'false';
  }
  goCnb(): void {
    document.forms['avocatForm'].submit();
  }

  setTitle(): string {

    console.log("length cibles", this.dashboard.headerEntreprise.values.cibles.value.length)

    if(this.dashboard.headerEntreprise.values.cibles.value.length == 2) {
      return "Entreprise éligible à de l’Affacturage et du Crédit-Bail Mobilier";
    }
    else {
      let cible = this.dashboard.headerEntreprise.values.cibles.value[0];
      console.log("tototo", cible)
      if(cible=='KCONFCCO' || cible=='KCONFCCT' || cible=='KCONFPRO' || cible=='KCONFCAE' || cible=='KCONFCCD') {
        return "Entreprise éligible à de l’Affacturage";
      }
      else if(cible=='KCONCCCO' || cible=='KCONCPRO') {
        return "Entreprise éligible à du Crédit-Bail Mobilier";
      }

    }

  }

  getNomAvocat(): string {
    try {
      return this.dashboardDirigeants.values.dirigeants.value[0].nom;
    } catch (ex) {}
    return '';
  }

  getPrenomAvocat(): string {
    try {
      return this.dashboardDirigeants.values.dirigeants.value[0].prenom;
    } catch (ex) {}
    return '';
  }

  appetenceClicked(): boolean {
    this.isAppetence = !this.isAppetence;
    const modalRef = this.modalService.open(NgbdModalContent, { centered: true } );
    modalRef.componentInstance.nomEntreprise = this.raisonSociale;
    modalRef.componentInstance.dashboard = this.dashboard;
    return this.isAppetence;
  }

  callAvocat(): void {
    this.webscraping = true;
    this.counter = 0
    this.dashboardService.getAvocat(this.dashboard.webScraping.avocatJobId,this.dashboard.headerEntreprise.values.siret.value)
      .subscribe(d => {
        this.counter++;
        if (d.status === 'OK') {
          this.resultatAvocat = d;
          this.dashboardIdentityEntreprise.values.avocatIdentifie = {
            value: (d.avocats.length === 1 && d.avocats[0] && d.avocats[0].avocat ? 'true' : 'false'),
            source: 'cnb'
          };
          this.webscraping = false;
        } else if (d.status === 'PENDING' && this.counter < this.MAX_NB_RETRY) {
          timer(this.POLLING_DELAY).subscribe(d => {
            this.callAvocat();
          });
        } else {
          this.webscraping = false;
        }
      });
  }

  showLocalisation(): boolean {
    let out = false;
    if (this.dashboardLocalisation) {
      const fieldList = [this.dashboardLocalisation.values.numeroTelephone,
      this.dashboardLocalisation.values.numeroTelephone,
      this.dashboardLocalisation.values.latitude,
      this.dashboardLocalisation.values.longitude
      ];
      for (const field of fieldList) {
        if (field && field.value) {
          out = true;
          break;
        }
      }
    }
    return out;
  }

  ngOnInit() {
    this.route.data
      .subscribe((data: { dasboardResolverService: IDashboardEntreprise }) => {
        this.dashboard = data.dasboardResolverService;
        this.raisonSociale = this.dashboard.headerEntreprise.values.raisonSociale.value;
        if (this.dashboard.webScraping) {
          this.callAvocat();
        }
        this.getDifferentWidgets();
        const crumb: Crumb = {
              path: '/entreprises/',
              param: this.dashboard.headerEntreprise.values.raisonSociale.value,
              label: this.dashboard.headerEntreprise.values.raisonSociale.value,
              url: '/entreprises/' + (this.dashboard.headerEntreprise.values.waldec?this.dashboard.headerEntreprise.values.waldec.value:this.dashboard.headerEntreprise.values.siret.value)
            };

        this.breadcrumbService.refreshBreadcrumb(crumb);
        this.raisonSociale = this.dashboard.headerEntreprise.values.raisonSociale.value;

        if(this.raisonSociale.length > 50) {
          this.styleTitle = "NameBig";
        }
        else {
          this.styleTitle="Name";
        }

    });

    this.userCustomizationService.getFullUser(this.userService.userCustomization).subscribe(user => {

      this.userService.userCustomization = user;

      let mesFavoris = this.favorisPortefeuilleService.getFavoris(this.userService.userCustomization.guid);
      let mesPortefeuilles = this.favorisPortefeuilleService.getPortefeuille(this.userService.userCustomization.guid);

      forkJoin([mesFavoris, mesPortefeuilles]).subscribe(results => {

        let favoris = results[0];
        let portefeuilles = results[1];
        favoris.forEach(favori => {
          if(this.dashboard.headerEntreprise.values.siret && favori.siret == this.dashboard.headerEntreprise.values.siret.value) {
            this.isFavori = true;
            this.myFavori = favori;
          }
          else if (this.dashboard.headerEntreprise.values.waldec && favori.waldec == this.dashboard.headerEntreprise.values.waldec.value) {
            this.isFavori = true;
            this.myFavori = favori;
          }
        });
        portefeuilles.forEach(portefeuille => {
          if(this.dashboard.headerEntreprise.values.siret && portefeuille.siret == this.dashboard.headerEntreprise.values.siret.value) {
            this.isPortefeuille = true;
            this.myPortefeuille = portefeuille;
          }
          else if (this.dashboard.headerEntreprise.values.waldec && portefeuille.waldec == this.dashboard.headerEntreprise.values.waldec.value) {
            this.isPortefeuille = true;
            this.myPortefeuille = portefeuille;
          }
        });
      });
    });
    this.showLocalisationWidget = this.showLocalisation();
    /*let rS = "\""+this.raisonSociale+"\"";
    this.queryRaisonSociale.push(rS);
    this.newsService.getNews(this.queryRaisonSociale).subscribe(news =>{

      for (let i = 0; i < news.length; i++) {
        news[i].raisonSociale = news[i].raisonSociale.slice(1, news[i].raisonSociale.length - 1);
      }
      this.articles = news.slice(0, 6);
    });*/

  }

  ngAfterViewInit() {

  }

  removeEmptyCode(s: String):String {
    let regExp=new RegExp("^0 - ");
    return s.replace(regExp,"");
  }

  getDateBilan(date: any): Date {
    return new Date(date.millisSinceEpoch);
  }
  getMarginMenu():Number {
    return document.getElementById("header").clientHeight-14;
  }

  getMarginContent():Number {
    return document.getElementById("header").clientHeight;
  }
  getDifferentWidgets(): void {

    this.dashboard.widgets.forEach( widget => {
      if (widget.type === 'Identity') {
        this.dashboardIdentityEntreprise = widget;
      }
      if (widget.type === 'Leaders') {
        this.dashboardDirigeants = widget;
      }
      if (widget.type.indexOf('KPI') >= 0 && widget.values) {
        this.dashboardKPI = widget;
        this.devise = this.getDevise();
      }
      if (widget.type === 'Localisation') {
        this.dashboardLocalisation = widget;
      }
      if (widget.type === 'Evenement') {
        this.dashboardEvt=widget;
      }
      if(widget.type == "Comptes") {
        this.dashboardBilan = widget;
        if (widget.values) {
          this.dashboardBilan.values.bilans.value.nomPdf = "https://s3-eu-west-1.amazonaws.com/batica-cdn/balo/" + widget.values.bilans.value.nomPdf;
        }
      }
    });
/*
    this.dashboardKPI = {
      values: {
        devise: 'EUR',
      //  annee: '2018',
    //    chiffreAffaireNet: 150345,
        effectif: 11212,
        disponibilitesNettes: 1540003,
        totalActifImmobilisationsNet: 10546535,
      //  totalDettesBrutes: 657888,
        valeurAjoute: 66667,
        excedentBrutExploitation: 12878888,
        capaciteAutofinancement: 65733,
        rentabiliteNette: 4553,
        fondRoulementNetGlobal: 77773,
        besoinsFondsRoulement: 8777,
        tresorieNette: 12000000,
        rendemantCapitauxPropres: 88,
        rendementRessourcesDurablesNettes: 64
      }
    };
*/
    if (!this.dashboardKPI||!this.dashboardKPI.values) {
      this.dashboardKPI={
        type:"KPI_C",
        values:{}
      }
    }
    if (!this.dashboardEvt||!this.dashboardEvt.values||!this.dashboardEvt.values.evenements) {
      this.dashboardEvt={
        values:{
          evenements:{
            value:[],
            source:''
          }
        }
      }
    }

  }


  getDevise():String {
    try {
      if (!this.dashboardKPI.values.devise) {
        return "";
      }
      let n: number = 0;
      let devise = n.toLocaleString("fr", {style: "currency", currency: this.dashboardKPI.values.devise.value});
      return devise.substring(devise.length - 1);
    }catch (ex) {
      return "EUR";
    }
  }

  dateSource(field: any): string {

    for (let source of this.dashboard.sources) {

      let day;
      let month;

      if(source.dateMajSource.day.toString().length == 1) {
        day = "0" + source.dateMajSource.day.toString();
      }
      else {
        day = source.dateMajSource.day;
      }

      if(source.dateMajSource.month.toString().length == 1) {
        month = "0" + source.dateMajSource.month.toString();
      }
      else {
        month = source.dateMajSource.month;
      }

      if(source.nomSource == field.source) {
        return day + "/" + month + "/" + source.dateMajSource.year;
      }

    }

    return "date pas trouvée";

  }

  addFavoris(): void {

    let favorisRequest: FavorisRequest;

    favorisRequest = {
      "waldec": !this.dashboard.headerEntreprise.values.waldec ? "" : this.dashboard.headerEntreprise.values.waldec.value,
      "siret": !this.dashboard.headerEntreprise.values.siret ? "" : this.dashboard.headerEntreprise.values.siret.value,
      "idUtilisateur": this.userService.userCustomization.guid
    }

    if(this.isFavori == false) {
     this.favorisPortefeuilleService.postFavoris(favorisRequest).subscribe(data => {
       this.isFavori = true;
       this.myFavori = data;
     });
    }
    else {
     this.favorisPortefeuilleService.deleteFavori(this.myFavori.guid).subscribe(data => {
       this.isFavori = false;
     });
    }
  }

  addPortefeuilles(): void {

    let portefeuilleRequest: PortefeuilleRequest;

    portefeuilleRequest = {
      "waldec": !this.dashboard.headerEntreprise.values.waldec ? "" : this.dashboard.headerEntreprise.values.waldec.value,
      "siret": !this.dashboard.headerEntreprise.values.siret ? "" : this.dashboard.headerEntreprise.values.siret.value,
      "idUtilisateur": this.userService.userCustomization.guid
    }

    if(this.isPortefeuille == false) {
     this.favorisPortefeuilleService.postPortefeuille(portefeuilleRequest).subscribe(data => {
       this.isPortefeuille = true;
       this.myPortefeuille = data;
     });
    }

    else {
     this.favorisPortefeuilleService.deletePortefeuille(this.myPortefeuille.guid).subscribe(data => {
       this.isPortefeuille = false;
     });
    }
  }

  getLocalite(dirigeant:any):String {
    if (dirigeant.villeNaissance) {
      if (!dirigeant.paysNaissance||dirigeant.paysNaissance.toLowerCase()=='france') {
        return dirigeant.villeNaissance;
      }else {
        return dirigeant.paysNaissance;
      }
    }else if (dirigeant.paysNaissance) {
      return dirigeant.paysNaissance;
    }else {
      return "Non renseigné";
    }

  }

  getDateNaissance(dirigeant:any):String {
      if (dirigeant.dateNaissance) {
        return dirigeant.dateNaissance;
      }else {
        return "Non renseigné";
      }

    }

    public getValue(value:String):String {
      if (!value) {
        return "Donnée indisponible"
      }else {
        return value;
      }
    }

}
