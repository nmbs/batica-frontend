import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-kpi-a',
  templateUrl: './kpi-a.component.html',
  styleUrls: ['../dashboard.component.scss']
})
export class KpiAComponent implements OnInit {
  @Input()
  dashboardKPI: any;
  @Input()
  dashboardBilan: any;
  @Input()
  devise: String;

  annee: String;

  constructor() {
  }

  ngOnInit() {
    if (this.dashboardKPI.values.annee) {
      this.annee = this.dashboardKPI.values.annee.value;
    }

  }

  getDateBilan(date: any): Date {
    return new Date(date.millisSinceEpoch);
  }

  getAnnee(): String {
    if (this.dashboardKPI.values.annee) {
      return 'en ' + this.dashboardKPI.values.annee.value;
    } else {
      return '';
    }
  }

}
