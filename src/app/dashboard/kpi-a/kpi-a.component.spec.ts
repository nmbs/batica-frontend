import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KpiAComponent} from './kpi-a.component';
import {NgbCarouselConfig, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {KPIPipe} from '../kpi.pipe';
import {UnitePipe} from '../unite.pipe';
import {FormsModule} from '@angular/forms';
import {TestUtils} from '../../test-utils/test-utils';

describe('KpiAComponent', () => {
  let component: KpiAComponent;
  let fixture: ComponentFixture<KpiAComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KpiAComponent, KPIPipe, UnitePipe],
      providers: [NgbCarouselConfig],
      imports: [FormsModule, NgbModule]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiAComponent);
    component = fixture.componentInstance;
    component.dashboardBilan = new TestUtils().getMockDashboardBilan();
    component.dashboardKPI = new TestUtils().getMockDashboardKPI();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();

  });
});
