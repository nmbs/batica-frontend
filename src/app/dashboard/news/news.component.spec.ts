import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {NewsDashboardComponent} from './news.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MockAppConfig} from '../../app.config.mock';
import {AppConfig} from '../../app.config';
import {CompanyService} from '../../company/company.service';
import {UserService} from '../../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {TestUtils} from '../../test-utils/test-utils';

describe('NewsComponent', () => {
  let component: NewsDashboardComponent;
  let fixture: ComponentFixture<NewsDashboardComponent>;
  const mockAppConfig = new MockAppConfig();
  const mockKeycloak = {
    profile: {}
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [NewsDashboardComponent],
      providers: [{
        provide: AppConfig,
        useValue: mockAppConfig
      }, CompanyService, UserService, {provide: KeycloakService, useValue: new TestUtils().getKeycloakServiceSpy()}],
      imports: [HttpClientTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
