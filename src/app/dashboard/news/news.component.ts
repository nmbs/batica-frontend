import { Component, OnInit, Input, ChangeDetectorRef } from '@angular/core';
import { ArrayType } from '@angular/compiler/src/output/output_ast';
import { IArticle } from '../../_shared/news';
import { NewsService } from '../../_shared/news.service';
import { DashboardComponent } from '../dashboard.component';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss']
})
export class NewsDashboardComponent implements OnInit {
  @Input() raisonSociale: string;
  news: Array<IArticle>;
  articles: IArticle[];
  queryRaisonSociale: Array<string> = [];

  constructor(private newsService: NewsService) { }

  ngOnInit() {
    this.raisonSociale = "\""+this.raisonSociale+"\"";
    this.queryRaisonSociale.push(this.raisonSociale);
    this.newsService.getNews(this.queryRaisonSociale).subscribe(news =>{
      news.forEach(article => {
        article.raisonSociale = article.raisonSociale.slice(1, article.raisonSociale.length - 1);
      })
      this.articles = news.slice(0, 6);
    });
  }



  calculTime(article: IArticle): any {

    let date = new Date(article.publishedAt);

    if( ((Date.now() - date.getTime()) / 3600000) < 24) {
      return Date.now() - date.getTime();
    }

    return date.getTime();


  }

}
