import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiKComponent } from './kpi-k.component';
import {NgbCarouselConfig, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {KPIPipe} from '../kpi.pipe';
import {FormsModule} from '@angular/forms';
import {UnitePipe} from '../unite.pipe';
import {TestUtils} from '../../test-utils/test-utils';

describe('KpiKComponent', () => {
  let component: KpiKComponent;
  let fixture: ComponentFixture<KpiKComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KpiKComponent, KPIPipe, UnitePipe],
      providers: [NgbCarouselConfig],
      imports: [FormsModule, NgbModule]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiKComponent);
    component = fixture.componentInstance;
    component.dashboardBilan = new TestUtils().getMockDashboardBilan();
    component.dashboardKPI = new TestUtils().getMockDashboardKPI();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
