import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'kpi'})
export class KPIPipe implements PipeTransform {
  transform(kpi: any, devise: String, defaultValue: String): String {
    if (!defaultValue) {
      defaultValue = '-';
    }
    try {
      if (!devise) {
        devise = '';
      }
      if (!kpi || !kpi.value) {
        return defaultValue;
      }
      const value = Number(kpi.value);
      const valueAbs = Math.abs(value);

      if (valueAbs > 1000000000) {
        return (Math.round(value / 100000000.0) / 10).toLocaleString('fr', {minimumFractionDigits: 1});
      } else if (valueAbs > 1000000) {
        return (Math.round(value / 100000.0) / 10).toLocaleString('fr', {minimumFractionDigits: 1});
      } else {
        return value.toLocaleString('fr') + ' ' + devise;
      }
    } catch (ex) {
      return defaultValue;
    }
  }
}
