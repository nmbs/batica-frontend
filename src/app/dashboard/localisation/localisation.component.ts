import {Component, Input, OnInit} from '@angular/core';
import * as leaf from 'leaflet';
import {ILocalisation} from '../widgets/ILocalisation';

@Component({
  selector: 'app-localisation',
  templateUrl: './localisation.component.html',
  styleUrls: ['./localisation.component.scss']
})
export class LocalisationComponent implements OnInit {

  @Input() dashboardLocalisation: ILocalisation;

  public numeroTelephone;
  public website;
  public domain;
  public showmap = false;
  public showInfos = false;

  constructor() {
  }

  ngOnInit() {

    if (this.dashboardLocalisation.values.numeroTelephone && this.dashboardLocalisation.values.numeroTelephone.value) {
      this.numeroTelephone = this.dashboardLocalisation.values.numeroTelephone.value;
      this.showInfos = true;
    }
    if (this.dashboardLocalisation.values.website && this.dashboardLocalisation.values.website.value) {
      const matches = this.dashboardLocalisation.values.website.value.match(/^https?\:\/\/([^\/?#]+)(?:[\/?#]|$)/i);
      this.domain = matches && matches[1];
      this.website = this.dashboardLocalisation.values.website.value;
      this.showInfos = true;
    }

    const icon = leaf.icon({
      iconUrl: '/assets/images/leaf-marker.png',
      //iconRetinaUrl: '/assets/images/leaf-marker-2x.png',
      shadowUrl: '/assets/images/leaf-marker-shadow.png'
    });

    const satLayer = leaf.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}',
      {attribution: 'Tiles © Esri — Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community'});

    const mapLayer = leaf.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {attribution: 'CALF'});
    const baseLayers = {
      'satelite': satLayer,
      'map': mapLayer
    };
    if (this.dashboardLocalisation.values.latitude && this.dashboardLocalisation.values.longitude) {
      const localMap = leaf.map('map', {
        zoomControl: false,
        zoom: 18,
        center: [this.dashboardLocalisation.values.latitude.value, this.dashboardLocalisation.values.longitude.value],
        layers: [satLayer, mapLayer]
      });
      leaf.marker([this.dashboardLocalisation.values.latitude.value,
        this.dashboardLocalisation.values.longitude.value], {icon: icon}).addTo(localMap);
      leaf.control.layers(baseLayers).addTo(localMap);
      this.showmap = true;
    }
  }
}
