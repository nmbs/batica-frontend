import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LocalisationComponent} from './localisation.component';

describe('LocalisationComponent', () => {
  let component: LocalisationComponent;
  let fixture: ComponentFixture<LocalisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LocalisationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocalisationComponent);
    component = fixture.componentInstance;
    component.dashboardLocalisation = {
      titre: '',
      type: '',
      values: {
        latitude: {
          value: 10,
          source: ''
        }
        ,
        longitude: {
          value: 20,
          source: ''

        }
        ,
        website: {
          value: '',
          source: ''
        }
        ,
        numeroTelephone: {
          value: '02 22 22 22 22',
          source: ''
        }
      }
  }
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
