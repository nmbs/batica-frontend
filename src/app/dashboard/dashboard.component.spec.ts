import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {DashboardComponent, NgbdModalContent} from './dashboard.component';
import {KpiAComponent} from './kpi-a/kpi-a.component';
import {KpiBComponent} from './kpi-b/kpi-b.component';
import {KpiSComponent} from './kpi-s/kpi-s.component';
import {KpiCComponent} from './kpi-c/kpi-c.component';
import {KpiKComponent} from './kpi-k/kpi-k.component';
import {LocalisationComponent} from './localisation/localisation.component';
import {NgbDropdownConfig, NgbModule, NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {DashboardService} from './dashboard.service';
import {NewsDashboardComponent} from './news/news.component';
import {KPIPipe} from './kpi.pipe';
import {UnitePipe} from './unite.pipe';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ActivatedRoute} from '@angular/router';
import {AppConfig} from '../app.config';
import {MockAppConfig} from '../app.config.mock';
import {UserService} from '../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {TestUtils} from '../test-utils/test-utils';
import {CompanyService} from '../company/company.service';
import {RouterTestingModule} from '@angular/router/testing';
import {ActivatedRouteMock} from '../list/list.component.spec';
import { NgbModalStack } from '../../../node_modules/@ng-bootstrap/ng-bootstrap/modal/modal-stack';


describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let activatedRoute: ActivatedRouteMock;
  activatedRoute = new ActivatedRouteMock();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent, KpiAComponent, KpiBComponent, KpiSComponent
        , KpiCComponent, KpiKComponent, LocalisationComponent, NewsDashboardComponent, KPIPipe, UnitePipe, NgbdModalContent ],
      providers: [DashboardService, {provide: ActivatedRoute, useValue: new TestUtils().getMockActivatedRoute()}, {
        provide: AppConfig,
        useValue: new MockAppConfig()
      }, UserService, {
        provide: KeycloakService,
        useValue: new TestUtils().getKeycloakServiceSpy()
      }, CompanyService, NgbDropdownConfig, NgbModal, NgbModalStack ],
      imports: [NgbModule, HttpClientTestingModule, RouterTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  /* it('should create', () => {
    expect(component).toBeTruthy();
  }); */
});
