import {Pipe, PipeTransform} from '@angular/core';

@Pipe({name: 'unite'})
export class UnitePipe implements PipeTransform {
  transform(kpi: any, devise: String, annee: String, forceDevise: boolean): String {
    if (!kpi || !kpi.value) {
      return '';
    }

    if (!devise) {
      devise = '';
    }
    let anneeS = '';
    if (annee) {
      anneeS = ' en ' + annee;
    }
    const value = Math.abs(Number(kpi.value));
    if (value > 1000000000) {
      if ((Math.round(value / 100000000.0) / 10) >= 2) {
        return 'milliards ' + devise + anneeS;
      } else {
        return 'milliard ' + devise + anneeS;
      }
    } else if (value > 1000000) {
      if ((Math.round(value / 100000.0) / 10) >= 2) {
        return 'millions ' + devise + anneeS;
      } else {
        return 'million ' + devise + anneeS;
      }
    } else {
      return forceDevise ? devise + anneeS : anneeS;
    }
  }
}
