import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';
import {DashboardService} from './dashboard.service';
import {IDashboardEntreprise} from './dashboard-entreprise';

@Injectable({
  providedIn: 'root'
})
export class DashboardResolverService implements Resolve<any> {

  constructor(private dashboardService: DashboardService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IDashboardEntreprise> {
    return this.dashboardService.get(route.params['siretWaldec']);
  }
}
