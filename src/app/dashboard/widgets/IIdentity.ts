export interface IIdentity {
  titre: string,
  type: string,
  values: {
    raisonSociale: {
      value: string,
      source: string
    },
    capitalSocial: {
      value: string,
      source: string
    },
    codeNAF: {
      value: string,
      source: string
    },
    labelNAF: {
      value: string,
      source: string
    },
    formeJuridique: {
      value: string,
      source: string
    },
    codeFormeJuridique: {
      value: string,
      source: string
    },
    enseigne: {
      value: string,
      source: string
    },
    sigle: {
      value: string,
      source: string
    },
    adresse: {
      value: string,
      source: string
    },
    avocatIdentifie: {
      value: string,
      source: string
    }
  }

}
