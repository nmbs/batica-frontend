export interface Header {
  titre: string;
  type: string;
  values: {
    raisonSociale: {
      value: string,
      source: string
    },
    siret: {
      value: string,
      source: string
    },
    waldec: {
      value: string,
      source: string
    },
    isin: {
      value: string,
      source: string
    },
    isBlacklist: {
      value: boolean,
      source: string
    },
    scoreGroupe: {
      value: string,
      source: string
    },
    isEcoResponsable: {
      value: boolean,
      source: string
    },
    isCoteBourse: {
      value: boolean,
      source: string
    },
    cibles: {
      value: string[],
      source: string
    }
  };
}
