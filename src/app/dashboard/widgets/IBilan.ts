export interface IBilan {
  titre: string,
  type: string,
  values: {
    bilans: {
      value: {
        date: {
          millisSinceEpoch: string,
          daysSinceEpoch: string,
          month: string,
          year: string,
          day: string
        },
        nomPdf: string
      },
      source: string
    }
  }
}
