export interface ILocalisation {
  titre: string;
  type: string;
  values: {
    latitude: {
      value: number;
      source: string
    },
    longitude: {
      value: number,
      source: string
    },
    website: {
      value: string,
      source: string
    },
    numeroTelephone: {
      value: string,
      source: string
    }
  };
}
