import {inject, TestBed} from '@angular/core/testing';

import {DashboardService} from './dashboard.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppConfig} from '../app.config';
import {MockAppConfig} from '../app.config.mock';
import {KpiAComponent} from './kpi-a/kpi-a.component';
import {KpiBComponent} from './kpi-b/kpi-b.component';
import {KpiKComponent} from './kpi-k/kpi-k.component';
import {KpiSComponent} from './kpi-s/kpi-s.component';
import {UnitePipe} from './unite.pipe';
import {KPIPipe} from './kpi.pipe';

describe('DashboardService', () => {
  beforeEach(() => {
    const mockAppConfig = new MockAppConfig();
    TestBed.configureTestingModule({
      providers: [DashboardService, {provide: AppConfig, useValue: mockAppConfig}],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([DashboardService], (service: DashboardService) => {
    expect(service).toBeTruthy();
  }));
});
