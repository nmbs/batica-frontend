import {Injectable} from '@angular/core';
import {IDashboardEntreprise} from './dashboard-entreprise';
import {Observable, of, throwError as observableThrowError} from 'rxjs';
import {AppConfig} from '../app.config';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {ICompany} from '../company/company';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private _http: HttpClient, private appConfig: AppConfig) {
  }

  private dashboardUrl = this.appConfig.config.dashboardUrl + '/entreprises';
  private webscrapringUrl = this.appConfig.config.dashboardUrl + '/avocats';

  get(siretWaldec: string): Observable<IDashboardEntreprise> {


    return this._http.get<IDashboardEntreprise>(this.dashboardUrl, {
      params: {
        siret: siretWaldec
      }
    }).pipe(
      catchError(this.handleError));

  }

  getAvocat(jobId: string, siret: string): Observable<any> {
    return this._http.get<IDashboardEntreprise>(this.webscrapringUrl, {
      params: {
        jobId: jobId,
        siret: siret
      }
    }).pipe(
      catchError(this.handleError));
  }

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return observableThrowError(err.message);
  }

  getCompaniesForFavPorte(listSiretWaldec: Map<string, string>): Observable<ICompany[]> {
    if (listSiretWaldec.size === 0) {
      return of([]);
    }
    let parameters = new HttpParams();
    listSiretWaldec.forEach((value: string, key: string) => {

      parameters = parameters.append('sirets', key);

    });

    return this._http.get<ICompany[]>(this.dashboardUrl + '/favoris', {params: parameters})
      .pipe(
        catchError(this.handleError));
  }

}
