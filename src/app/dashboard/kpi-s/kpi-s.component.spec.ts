import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {KpiSComponent} from './kpi-s.component';
import {NgbCarouselConfig, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {KPIPipe} from '../kpi.pipe';
import {FormsModule} from '@angular/forms';
import {UnitePipe} from '../unite.pipe';
import {TestUtils} from '../../test-utils/test-utils';

describe('KpiSComponent', () => {
  let component: KpiSComponent;
  let fixture: ComponentFixture<KpiSComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KpiSComponent, KPIPipe, UnitePipe],
      providers: [NgbCarouselConfig],
      imports: [FormsModule, NgbModule]

    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(KpiSComponent);
    component = fixture.componentInstance;
    component.dashboardBilan = new TestUtils().getMockDashboardBilan();
    component.dashboardKPI = new TestUtils().getMockDashboardKPI();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
