import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KpiCComponent } from './kpi-c.component';
import {NgbCarouselConfig, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {KPIPipe} from '../kpi.pipe';
import {FormsModule} from '@angular/forms';
import {UnitePipe} from '../unite.pipe';
import {TestUtils} from '../../test-utils/test-utils';

describe('KpiCComponent', () => {
  let component: KpiCComponent;
  let fixture: ComponentFixture<KpiCComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KpiCComponent, KPIPipe, UnitePipe],
      providers: [NgbCarouselConfig],
      imports: [FormsModule, NgbModule]

    })
      .compileComponents();
  }));


  beforeEach(() => {
    fixture = TestBed.createComponent(KpiCComponent);
    component = fixture.componentInstance;
    component.dashboardBilan = new TestUtils().getMockDashboardBilan();
    component.dashboardKPI = new TestUtils().getMockDashboardKPI();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
