import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { KpiBComponent } from './kpi-b.component';
import {NgbCarouselConfig, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {KPIPipe} from '../kpi.pipe';
import {FormsModule} from '@angular/forms';
import {UnitePipe} from '../unite.pipe';
import {TestUtils} from '../../test-utils/test-utils';

describe('KpiBComponent', () => {
  let component: KpiBComponent;
  let fixture: ComponentFixture<KpiBComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [KpiBComponent, KPIPipe, UnitePipe],
      providers: [NgbCarouselConfig],
      imports: [FormsModule, NgbModule]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KpiBComponent);
    component = fixture.componentInstance;
    component.dashboardBilan = new TestUtils().getMockDashboardBilan();
    component.dashboardKPI = new TestUtils().getMockDashboardKPI();
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
