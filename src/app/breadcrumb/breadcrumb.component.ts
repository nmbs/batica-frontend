import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BreadcrumbService} from './breadcrumb.service';

@Component({
  selector: 'breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class BreadcrumbComponent implements OnInit {

  breadcrumbs$;

// Build your breadcrumb starting with the root route of your current activated route

  constructor(private _http: HttpClient, private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.breadcrumb$.subscribe(breadcrumb => {
      this.breadcrumbs$ = breadcrumb;
    });
  }

  ngOnInit() {
  }

}
