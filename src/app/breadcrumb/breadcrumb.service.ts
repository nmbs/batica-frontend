import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Crumb} from './breadcrumb';

@Injectable({
  providedIn: 'root'
})
export class BreadcrumbService {

  constructor() {
    let crumb: Crumb = {
      path: '/',
      param: '',
      label: 'Accueil',
      url: '/'
    };
    this.breadcrumb.push(crumb);
    this.history.next(this.breadcrumb);
  }

  private history = new Subject<Crumb[]>();
  private breadcrumb: Crumb[] = [];

  // Observable string streams
  breadcrumb$ = this.history.asObservable();

  // Service message commands

  refreshBreadcrumb(crumbToAdd: Crumb) {

    let conditionAdd: boolean = true;
    let indexToDelete;

    this.breadcrumb.forEach((crumb, index) => {
      if (crumb.path == crumbToAdd.path) {
        conditionAdd = false;
        indexToDelete = index;
      }
    });

    if (conditionAdd) {
      this.breadcrumb.push(crumbToAdd);
      this.history.next(this.breadcrumb);
      return;
    }

    else if (!conditionAdd && this.breadcrumb[indexToDelete].url !== crumbToAdd.url) {
      this.breadcrumb = this.breadcrumb.slice(0, indexToDelete);
      this.breadcrumb.push(crumbToAdd);
      this.history.next(this.breadcrumb);
    }

    else {

      this.breadcrumb = this.breadcrumb.slice(0, indexToDelete + 1);
      this.history.next(this.breadcrumb);
    }
  }

}
