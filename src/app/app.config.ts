import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {throwError} from 'rxjs';
import {Config} from './config';

@Injectable()
export class AppConfig {

  public config: Config = null;

  constructor(private http: HttpClient) {

  }

  getConfig(): Config {
    return this.config;
  }

  loadConfig() {
    return this.http.get<Config>('/assets/configuration/config.json');
  }

  public load(): Promise<Config> {
    return new Promise((resolve, reject) => {
      this.loadConfig().subscribe(
        data => {
          this.config = data;
          resolve(data);
        },
        error => {
          console.error('Error loading config file');
          return throwError(error);
        }
      );
    });
  }
}
