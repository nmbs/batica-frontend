import {KeycloakService} from 'keycloak-angular';
import {AppConfig} from './app.config';
import {Config} from './config';

export function initConfig(config: AppConfig): Promise<Config> {
  return config.load();
}

function initKeycloak(AppConfig: AppConfig, keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: AppConfig.config.keycloakConfig,
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe: false
          },
          bearerExcludedUrls: []
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
}

export function initializer(AppConfig: AppConfig, keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return initConfig(AppConfig).then(initKeycloak(AppConfig, keycloak));
  };
}
