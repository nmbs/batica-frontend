import {async, TestBed} from '@angular/core/testing';
import {NotificationComponent} from './notification.component';

describe('NotificationComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NotificationComponent
      ],
    }).compileComponents();
  }));
});
