export interface IArticle {
  source: ISourceArticle,
  raisonSociale: string,
  author: string,
  title: string,
  description: string,
  url: string,
  urlToImage: string,
  publishedAt: string
}

export interface ISourceArticle {
  id: string,
  name: string
}
