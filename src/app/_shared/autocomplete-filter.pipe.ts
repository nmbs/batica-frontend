import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'autocompleteFilter'
})
export class AutocompleteFilterPipe implements PipeTransform {

  transform(list: any, search: string, field: string): any {
    if (!search) {
      return list;
    }

    let solution = list.filter(v => {
      if (!v) {
        return;
      }

      else if ((field === 'commune' || field === 'codePostal') && v.adresse[field] != undefined) {

        return v.adresse[field].toLowerCase().indexOf(search.toLowerCase()) !== -1;
      }

      else if ((v[field] == undefined || v[field] == null || v[field] == '') && field === 'siret') {

        return v['waldec'].toLowerCase().indexOf(search.toLowerCase()) !== -1;
      }

      else if (field === 'siret') {
        return v[field].toLowerCase().indexOf(search.toLowerCase()) !== -1;
      }

      else {
        return false;
      }

    });

    solution = this.removeDuplicates(solution, field);

    return solution;

  }

  removeDuplicates(myArr, field) {
    return myArr.filter((obj, pos, arr) => {

      if (field === 'codePostal' || field === 'commune') {
        return arr.map(mapObj =>
          mapObj.adresse[field]).indexOf(obj.adresse[field]) === pos;
      }

      else if ((obj[field] == undefined || obj[field] == null) && field === 'siret') {
        return arr.map(mapObj =>
          mapObj['waldec']).indexOf(obj['waldec']) === pos;
      }

      else if (field === 'siret') {
        return arr.map(mapObj =>
          mapObj[field]).indexOf(obj[field]) === pos;
      }

      else {
        return false;
      }

    });
  }

}
