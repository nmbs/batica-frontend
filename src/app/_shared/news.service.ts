import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError as observableThrowError,  Observable } from 'rxjs';
import { AppConfig } from '../app.config';
import { catchError, publishReplay, refCount } from 'rxjs/operators';
import { IArticle } from './news';

@Injectable({
  providedIn: 'root'
})
export class NewsService {

  urlNewsApi: string;

  constructor(private _http: HttpClient, 
    private appConfig: AppConfig) {
      this.urlNewsApi = appConfig.config.dashboardUrl + "/actualites";
    }

  getNews(raisonSociale: Array<string>): Observable<Array<IArticle>> {

    return this._http.get<Array<IArticle>>(this.urlNewsApi, {
      params: {
        raisonSociale: raisonSociale
      }
    }).pipe(
        catchError(this.handleError),publishReplay(1),refCount()
      );
  }

  private handleError(err: HttpErrorResponse) {

    console.log(err);
    return observableThrowError(err);
  }
}
