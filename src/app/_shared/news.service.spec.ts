import {inject, TestBed} from '@angular/core/testing';

import {NewsService} from './news.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MockAppConfig} from '../app.config.mock';
import {AppConfig} from '../app.config';

describe('NewsService', () => {
  const mockAppConfig = new MockAppConfig();
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewsService, {provide: AppConfig, useValue: mockAppConfig}],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([NewsService], (service: NewsService) => {
    expect(service).toBeTruthy();
  }));
});
