import {Pipe, PipeTransform} from '@angular/core';
import * as changeCase from 'change-case';

@Pipe({name: 'orderFilter'})
export class OrderFilter implements PipeTransform {

  transform(list: Array<string>, key: string): Array<Object> {
    let newStr: Array<string> = Array();
    let objectList: Array<Object> = Array();
    if (key == 'commune' || key == 'codePostal') {
      for (var i = 0; i <= list.length - 1; i++) {
        if (list[i]['adresse'][key] && !newStr.includes(list[i]['adresse'][key].toLowerCase())) {
          newStr.push(list[i]['adresse'][key].toLowerCase());
          objectList.push(list[i]);
        }
      }
      objectList.sort((a, b) => a['adresse'][key] < b['adresse'][key] ? -1 : a['adresse'][key] > b['adresse'][key] ? 1 : 0);
    }
    else {
      for (var i = 0; i <= list.length - 1; i++) {
        if (list[i][key] && !newStr.includes(list[i][key].toLowerCase())) {
          newStr.push(changeCase.titleCase(list[i][key]));
          objectList.push(list[i]);
        }
      }
      objectList.sort((a, b) => a[key] < b[key] ? -1 : a[key] > b[key] ? 1 : 0);
    }

    newStr.sort((a, b) => 0 - (a > b ? -1 : 1));

    return objectList;
  }
}
