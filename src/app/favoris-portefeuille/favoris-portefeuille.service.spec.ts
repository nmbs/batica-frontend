import {inject, TestBed} from '@angular/core/testing';

import {FavorisPortefeuilleService} from './favoris-portefeuille.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {TestUtils} from '../test-utils/test-utils';
import {MockAppConfig} from '../app.config.mock';
import {KeycloakService} from 'keycloak-angular';
import {CompanyService} from '../company/company.service';
import {AppConfig} from '../app.config';
import {SearchHistoryService} from '../history/search-history.service';
import {UserService} from '../user/user.service';

describe('FavorisPortefeuilleService', () => {
  beforeEach(() => {
    const appConfigSpy = new MockAppConfig();
    const keycloakServiceSpy = new TestUtils().getKeycloakServiceSpy();

    TestBed.configureTestingModule({
      providers: [SearchHistoryService, UserService, CompanyService, {
        provide: KeycloakService,
        useValue: keycloakServiceSpy
      }, {provide: AppConfig, useValue: appConfigSpy}],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([FavorisPortefeuilleService], (service: FavorisPortefeuilleService) => {
    expect(service).toBeTruthy();
  }));
});
