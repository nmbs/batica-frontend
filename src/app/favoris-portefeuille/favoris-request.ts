export interface FavorisRequest {
  siret: string,
  waldec: string,
  idUtilisateur: string
}
