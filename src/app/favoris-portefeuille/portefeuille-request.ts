export interface PortefeuilleRequest {
  siret: string,
  waldec: string,
  idUtilisateur: string
}
