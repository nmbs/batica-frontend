export interface Favoris {
  guid: string,
  date: string,
  siret: string,
  waldec: string,
  utilisateurId: string
}
