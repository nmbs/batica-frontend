import {Injectable} from '@angular/core';
import {Favoris} from './favoris';
import {FavorisRequest} from './favoris-request';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {AppConfig} from '../app.config';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {catchError, publishReplay, refCount} from 'rxjs/operators';
import {PortefeuilleRequest} from './portefeuille-request';
import {Portefeuille} from './portefeuille';

@Injectable({
  providedIn: 'root'
})
export class FavorisPortefeuilleService {

  urlFavoris: string;
  urlPortfeuille: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig) {
    this.urlFavoris = this.appConfig.config.customizationUrl + '/favoris';
    this.urlPortfeuille = this.appConfig.config.customizationUrl + '/portefeuilles';
  }

  postFavoris(favorisRequest: FavorisRequest): Observable<Favoris> {
    this.clearCacheFavori();
    return this._http.post<Favoris>(this.urlFavoris, favorisRequest)
      .pipe(
        catchError(this.handleError)
      );
  }

  favoris: Observable<Array<Favoris>>;

  getFavoris(guid: string): Observable<Array<Favoris>> {
    if (!this.favoris) {
      this.favoris = this._http.get<Array<Favoris>>(this.urlFavoris, {
        params: {
          guid: guid
        }
      }).pipe(
        catchError(this.handleError), publishReplay(1), refCount()
      );
    }
    return this.favoris;
  }

  clearCacheFavori() {
    this.favoris = null;
  }

  clearCachePortefeuille() {
    this.portefeuilles = null;
  }

  deleteFavori(isFavori: string): Observable<Favoris> {
    this.clearCacheFavori();
    return this._http.delete<Favoris>(this.urlFavoris, {
      params: {
        guid: isFavori
      }
    })
      .pipe(
        catchError(this.handleError)
      );
  }

  postPortefeuille(portefeuilleRequest: PortefeuilleRequest): Observable<Portefeuille> {
    this.clearCachePortefeuille();
    return this._http.post<Portefeuille>(this.urlPortfeuille, portefeuilleRequest)
      .pipe(
        catchError(this.handleError)
      );
  }


  deletePortefeuille(isPortefeuille: string): Observable<Portefeuille> {
    this.clearCachePortefeuille();
    return this._http.delete<Portefeuille>(this.urlPortfeuille, {
      params: {
        guid: isPortefeuille
      }
    })
      .pipe(
        catchError(this.handleError)
      );
  }

  portefeuilles: Observable<Array<Portefeuille>>;

  getPortefeuille(guid: string): Observable<Array<Portefeuille>> {
    if (!this.portefeuilles) {
      this.portefeuilles = this._http.get<Array<Portefeuille>>(this.urlPortfeuille, {
        params: {
          guid: guid
        }
      })
        .pipe(
          catchError(this.handleError), publishReplay(1), refCount()
        );
    }
    return this.portefeuilles;
  }

  private handleError(err: HttpErrorResponse) {

    console.log(err);
    return observableThrowError(err);
  }
}
