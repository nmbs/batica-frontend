export interface Portefeuille {
  guid: string,
  date: string,
  siret: string,
  waldec: string,
  utilisateurId: string,
  raisonSociale: string
}
