import {Injectable} from '@angular/core';
import {Config} from './config';

@Injectable()
export class MockAppConfig {

  public config: Config = {
    backendUrl: '', userUrl: '', searchHistoryUrl: '', customizationUrl: '', dashboardUrl: '',
    keycloakConfig: {url: '', realm: 'batica', clientId: 'batica-frontend'}
  };


  getConfig(): Config {
    return this.config;
  }

}
