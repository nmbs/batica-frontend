import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {FormsModule} from '@angular/forms';
import {SearchBoxComponent} from './search-box.component';
import {TestUtils} from '../test-utils/test-utils';
import {UserService} from '../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {TutorialComponent} from '../tutorial/tutorial.component';
import {SearchHistoryComponent} from '../tutorial/search-history/search-history.component';
import {MatCell, MatHeaderCell, MatHeaderRow, MatHeaderRowDef, MatRow, MatRowDef, MatTable} from '@angular/material';
import {HttpClient, HttpHandler} from '@angular/common/http';
import {MockAppConfig} from '../app.config.mock';
import {AppConfig} from '../app.config';
import {CompanyService} from '../company/company.service';
import {NewsComponent} from '../tutorial/news/news.component';

describe('SearchBoxComponent', () => {
  let component: SearchBoxComponent;
  let fixture: ComponentFixture<SearchBoxComponent>;

  beforeEach(async(() => {
    const keycloakServiceSpy = new TestUtils().getKeycloakServiceSpy();
    const appConfigMock = new MockAppConfig();
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule],
      declarations: [SearchBoxComponent, TutorialComponent, SearchHistoryComponent, MatTable
        , MatHeaderCell, MatCell, MatHeaderRow, MatHeaderRowDef, MatRow, MatRowDef, NewsComponent],
      providers: [UserService, TutorialComponent, HttpClient, HttpHandler, {
        provide: KeycloakService,
        useValue: keycloakServiceSpy
      }, {provide: AppConfig, useValue: appConfigMock}, CompanyService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
