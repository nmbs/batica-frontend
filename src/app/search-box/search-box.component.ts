import {Component, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Router} from '@angular/router';
import {UserService} from '../user/user.service';
import {TutorialComponent} from '../tutorial/tutorial.component';

// import { HttpClient } from '@angular/common/http';

enum Mode {
  empty,
  normal,
  tuto,
  tutoClosed,
  history
}

@Component({
  selector: 'batica-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {
  Mode = Mode;
  private searchDebounce = 300;
  private searchSubject = new Subject<string>();
  private entreprises = [];
  public searchTerm: string = '';
  private subscription;

  public mode: Mode;

  personFirstName = this.userService.personFirstName;

  public path;
  subscription2: any;

  constructor(private router: Router, private userService: UserService, private tutoComponent: TutorialComponent) {
    this.path = router.url;

  }

  ngOnInit(): void {
    this.mode = this.getMode();
    if (this.mode === Mode.tuto) {
      this.subscription = this.tutoComponent.OnHideTuto.subscribe(value => {
        if (value) {
          this.mode = Mode.tutoClosed;
        }
      });
    }
  }

  public s: string;

  private getMode(): Mode {
    var regex = new RegExp('#.*');
    this.s = this.router.url.replace(regex, '');
    if (this.s !== '/') {
      return Mode.normal;
    }
    if (this.userService.userCustomization.history && this.userService.userCustomization.history.length > 0) {
      return Mode.history;
    }

    if (!this.userService.userCustomization.tutorial) {
      return Mode.tuto;
    }
    return Mode.empty;
  }

  search(event): void {
    this.router.navigate(['list', this.searchTerm]);
  }

  ngOnDestroy() {
    this.tutoComponent.OnHideTuto.unsubscribe();
    this.tutoComponent.isFirstVisitWithoutTutorial.unsubscribe();
  }


}
