import {async, TestBed} from '@angular/core/testing';
import {AvatarComponent} from './avatar.component';

describe('AvatarComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AvatarComponent
      ],
    }).compileComponents();
  }));

});
