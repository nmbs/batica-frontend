import {Component} from '@angular/core';
import {UserService} from '../user/user.service';

@Component({
  selector: 'avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss']
})
export class AvatarComponent {
  pageTitle: string = 'Batica';
  personName = this.userService.personFirstName + ' ' + this.userService.personLastName;

  constructor(private userService: UserService) {
  }
}
