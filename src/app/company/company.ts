export interface ICompany {
  id: string,
  siret: string,
  waldec: string,
  raisonSociale: string,
  dirigeant: {
    nom: string,
    dateNaissance: string,
    communeNaissance: string,
    paysNaissance: string
  },
  adresse: {
    voie: string,
    codePostal: string,
    commune: string
  },
  eirl: boolean,
  activiteAuxiliaire: boolean,
  siege: boolean,
  statut: string,
  score: string,
  blacklist: boolean,
  isin: boolean,
  ecoResponsable: boolean,
  isFavori: string,
  isPortefeuille: string,
  isSelected: boolean
}
