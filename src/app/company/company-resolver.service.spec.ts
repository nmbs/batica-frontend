import {inject, TestBed} from '@angular/core/testing';

import {CompanyResolverService} from './company-resolver.service';
import {CompanyService} from './company.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {HttpClient} from '@angular/common/http';
import {AppConfig} from '../app.config';

describe('CompanyResolverService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let appConfig: AppConfig;

  beforeEach(() => {
    const appConfigSpy = jasmine.createSpyObj('AppConfig', ['config']);
    appConfigSpy.config.and.returnValue({backendUrl: ''});
    TestBed.configureTestingModule({
      providers: [CompanyResolverService, CompanyService, {provide: AppConfig, useValue: appConfigSpy}],
      imports: [HttpClientTestingModule]
    });

    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    appConfig = TestBed.get(AppConfig);
  });

  it('should be created', inject([CompanyResolverService], (service: CompanyResolverService) => {
    expect(service).toBeTruthy();
  }));
});
