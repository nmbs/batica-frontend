import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';

import {Observable} from 'rxjs';
import {CompanyService} from './company.service';
import {ICompany} from './company';

@Injectable({
  providedIn: 'root'
})
export class CompanyResolverService implements Resolve<any> {

  constructor(private companyService: CompanyService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICompany[]> {
    return this.companyService.getCompaniesBySiren(route.params['searchTerm']);
  }
}
