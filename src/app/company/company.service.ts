import {Observable, of, throwError as observableThrowError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ICompany} from './company';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {AppConfig} from '../app.config';
import {SearchHistory} from '../history/search-history';
import {Portefeuille} from '../favoris-portefeuille/portefeuille';
import {Favoris} from '../favoris-portefeuille/favoris';

@Injectable()
export class CompanyService {

  private handleError(err: HttpErrorResponse) {
    console.log(err.message);
    return observableThrowError(err.message);
  }

  constructor(private _http: HttpClient, private appConfig: AppConfig) {

  }

  private companiesUrl = this.appConfig.config.backendUrl;

  getCompaniesBySiren(searchTerm: string): Observable<ICompany[]> {

    return this._http.get<ICompany[]>(this.companiesUrl, {
      params: {
        r: searchTerm
      }
    }).pipe(
      catchError(this.handleError));
  }

  getCompaniesForHistory(listSiretWaldec: Array<SearchHistory>): Observable<ICompany[]> {
    if (listSiretWaldec.length === 0) {
      return of([]);
    }
    let parameters = new HttpParams();
    listSiretWaldec.forEach(element => {
      if (element.waldec && element.waldec.trim() !== '') {
        parameters = parameters.append('waldec', element.waldec);
      } else if (element.siret && element.siret.trim() !== '') {
        parameters = parameters.append('siret', element.siret);
      }
    });

    return this._http.get<ICompany[]>(this.companiesUrl, {params: parameters})
      .pipe(
        catchError(this.handleError));
  }

  getCompaniesForNews(favoris: Array<Favoris>, portefeuille: Array<Portefeuille>): Observable<ICompany[]> {
    if (favoris.length === 0 && portefeuille.length === 0) {
      return of([]);
    }
    let parameters = new HttpParams();
    favoris.forEach(element => {
      if (element.siret && element.siret.trim() !== '') {
        parameters = parameters.append('siret', element.siret);
      } else if (element.waldec && element.waldec.trim() !== '') {
        parameters = parameters.append('waldec', element.waldec);
      }
    });
    portefeuille.forEach(element => {
      if (element.siret && element.siret.trim() !== '') {
        parameters = parameters.append('siret', element.siret);
      } else if (element.waldec && element.waldec.trim() !== '') {
        parameters = parameters.append('waldec', element.waldec);
      }
    });

    return this._http.get<ICompany[]>(this.companiesUrl, {params: parameters})
      .pipe(
        catchError(this.handleError));
  }

}
