import {Component, OnInit} from '@angular/core';
import {AppConfig} from '../app.config';

@Component({
  selector: 'app-pending-creation',
  templateUrl: './pending-creation.component.html',
  styleUrls: ['./pending-creation.component.scss']
})
export class PendingCreationComponent implements OnInit {
  appConfig: AppConfig;
  url: string;

  constructor(private AppConfig: AppConfig) {
    this.appConfig = AppConfig;
  }

  ngOnInit() {
    let endodedUrl: string = encodeURI(document.URL.substring(0, document.URL.indexOf('/', 10)));
    this.url = this.appConfig.config.keycloakConfig.url + '/realms/' + this.appConfig.config.keycloakConfig.realm + '/protocol/openid-connect/logout?redirect_uri=' + endodedUrl;
  }


}
