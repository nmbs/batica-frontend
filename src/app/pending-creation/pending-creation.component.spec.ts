import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {PendingCreationComponent} from './pending-creation.component';
import {TestUtils} from '../test-utils/test-utils';
import {AppConfig} from '../app.config';
import {UserService} from '../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {MockAppConfig} from '../app.config.mock';

describe('PendingCreationComponent', () => {
  let component: PendingCreationComponent;
  let fixture: ComponentFixture<PendingCreationComponent>;

  beforeEach(async(() => {
    const appConfigMock = new MockAppConfig();
    const keycloakServiceSpy = new TestUtils().getKeycloakServiceSpy();
    TestBed.configureTestingModule({
      declarations: [PendingCreationComponent],
      providers: [UserService, {provide: AppConfig, useValue: appConfigMock}, {provide: KeycloakService, useValue: keycloakServiceSpy}]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingCreationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
