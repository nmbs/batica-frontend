import {inject, TestBed} from '@angular/core/testing';

import {SearchHistoryService} from './search-history.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppConfig} from '../app.config';
import {UserService} from '../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {CompanyService} from '../company/company.service';
import {TestUtils} from '../test-utils/test-utils';
import {MockAppConfig} from '../app.config.mock';

describe('SearchHistoryService', () => {
  beforeEach(() => {

    const appConfigSpy = new MockAppConfig();
    const keycloakServiceSpy = new TestUtils().getKeycloakServiceSpy();

    TestBed.configureTestingModule({
      providers: [SearchHistoryService, UserService, CompanyService, {
        provide: KeycloakService,
        useValue: keycloakServiceSpy
      }, {provide: AppConfig, useValue: appConfigSpy}],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([SearchHistoryService], (service: SearchHistoryService) => {
    expect(service).toBeTruthy();
  }));
});
