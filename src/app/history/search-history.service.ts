import {Injectable} from '@angular/core';
import {AppConfig} from '../app.config';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {SearchHistory} from './search-history';
import {catchError, map, mergeMap, tap} from 'rxjs/operators';
import {ICompany} from '../company/company';
import {UserCustomization} from '../user/user-customization';
import {CompanyService} from '../company/company.service';
import {UserService} from '../user/user.service';


class HistoryRequest {
  siret: string;
  waldec: string;
  idUtilisateur: string;

  constructor(siret: string, waldec: string, idUtilisateur: string) {
    this.siret = siret;
    this.waldec = waldec;
    this.idUtilisateur = idUtilisateur;
  }
}

@Injectable({
  providedIn: 'root'
})
export class SearchHistoryService {

  private searchHistoryUrl = this.appConfig.config.customizationUrl + '/historiques';
  private customizationUrl = this.appConfig.config.customizationUrl;

  constructor(private _http: HttpClient, private appConfig: AppConfig, private userService: UserService, private companyService: CompanyService) {
  }

  postCompanySearchHistory(company: ICompany): Observable<SearchHistory> {
    this.clearHistory();
    let rep = new HistoryRequest(company.siret, company.waldec, this.userService.userCustomization.guid);

    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    return this._http.post<SearchHistory>(this.searchHistoryUrl, rep, httpOptions)
      .pipe(
        tap((rep: SearchHistory) => console.log(`added hero w/ id=${rep}`)),
        catchError(this.handleError));
  }

  histories: Observable<SearchHistory[]>;

  getSearchHistory(id: string): Observable<SearchHistory[]> {
    if (!this.histories) {
      this.histories = this._http.get<SearchHistory[]>(this.searchHistoryUrl, {
        params: {
          guid: id
        }
      })
        .pipe(
          catchError(this.handleError)
        );
    }
    return this.histories;
  }

  clearHistory() {
    this.histories = null;
  }

  seachHistory: SearchHistory[];

  getFullSeachHistory(userId: string): Observable<ICompany[]> {
    return this.getSearchHistory(userId).pipe(mergeMap(history => {
      this.seachHistory = history;
      return this.companyService.getCompaniesForHistory(history);
    }), map(entreprises => {
      return this.sortSearchHistory(entreprises, this.seachHistory);
    }));
  }

  fillSearchHistory(user: UserCustomization): Observable<UserCustomization> {
    return this.getFullSeachHistory(user.guid).pipe(map(history => {
      user.history = history;
      return user;
    }));
  }

  private handleError(err: HttpErrorResponse) {

    console.log(err);
    return observableThrowError(err);
  }

  sortSearchHistory(entreprises: Array<ICompany>, historiques: Array<SearchHistory>): Array<ICompany> {
    let resultat = [];

    historiques.forEach(historique => {

      entreprises.forEach(entreprise => {
        if ((entreprise.siret && entreprise.siret != '' && historique.siret == entreprise.siret) || (entreprise.waldec && entreprise.waldec != '' && historique.waldec == entreprise.waldec)) {
          resultat.push(entreprise);
        }
      });
    });

    return resultat;

  }

}
