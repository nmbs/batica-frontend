export interface SearchHistory {
  id: string
  date: string
  siret: string
  waldec: string
  utilisateurId: string
}
