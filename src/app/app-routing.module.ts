import {RouterModule, Routes} from '@angular/router';
import {ListComponent} from './list/list.component';
import {NgModule} from '@angular/core';
import {TutorialComponent} from './tutorial/tutorial.component';
import {PendingCreationComponent} from './pending-creation/pending-creation.component';
import {CompanyResolverService} from './company/company-resolver.service';
import {UserResolverService} from './user/user-resolver.service';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DashboardResolverService} from './dashboard/dashboard-resolver';

const appRoutes: Routes = [
  {path: '', component: TutorialComponent, resolve: {userResolver: UserResolverService}},
  {path: 'list/:searchTerm', component: ListComponent, resolve: {companyResolver: CompanyResolverService}},
  {path: 'pending-creation', component: PendingCreationComponent},
  {path: 'entreprises/:siretWaldec', component: DashboardComponent, resolve: {dasboardResolverService: DashboardResolverService}}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      // { enableTracing: true } // <-- debugging purposes only
    )
    // other imports here
  ],
  exports: [
    RouterModule
  ],
  providers: [CompanyResolverService, UserResolverService, DashboardResolverService]
})
export class AppRoutingModule {
}
