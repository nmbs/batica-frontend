export interface User {
  nom: string
  prenom: string
  email: string
  tutorial: boolean,
  newUser: boolean
}
