import {inject, TestBed} from '@angular/core/testing';

import {UserCustomizationService} from './user-customization.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {SearchHistoryService} from '../history/search-history.service';
import {TestUtils} from '../test-utils/test-utils';
import {AppConfig} from '../app.config';
import {KeycloakService} from 'keycloak-angular';
import {UserService} from './user.service';
import {CompanyService} from '../company/company.service';
import {MockAppConfig} from '../app.config.mock';

describe('UserCustomizationService', () => {
  beforeEach(() => {
    const appConfigSpy = new MockAppConfig();
    const keycloakServiceSpy = new TestUtils().getKeycloakServiceSpy();
    TestBed.configureTestingModule({
      providers: [CompanyService, UserService, UserCustomizationService, SearchHistoryService, {
        provide: AppConfig,
        useValue: appConfigSpy
      }, {provide: KeycloakService, useValue: keycloakServiceSpy}],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([UserCustomizationService], (service: UserCustomizationService) => {
    expect(service).toBeTruthy();
  }));
});
