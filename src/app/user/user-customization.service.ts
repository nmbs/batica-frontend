import {Injectable} from '@angular/core';
import {User} from './user';
import {Observable, throwError as observableThrowError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, mergeMap, publishReplay, refCount} from 'rxjs/operators';
import {AppConfig} from '../app.config';
import {UserCustomization} from './user-customization';
import {SearchHistoryService} from '../history/search-history.service';

@Injectable({
  providedIn: 'root'
})
export class UserCustomizationService {

  private userUrl = this.appConfig.config.customizationUrl + '/utilisateurs';
  public guid: string;

  constructor(private _http: HttpClient, private appConfig: AppConfig, private searchHistoryService: SearchHistoryService) {
  }

  postUser(user: User): Observable<UserCustomization> {
    return this._http.post<UserCustomization>(this.userUrl, user)
      .pipe(
        catchError(this.handleError)
      );
  }

  getUser(email: string): Observable<UserCustomization> {
    if (!this.user) {
      this.user = this._http.get<UserCustomization>(this.userUrl, {
        params: {
          email: encodeURIComponent(email)
        }
      }).pipe(publishReplay(1), refCount());
    }
    return this.user;
  }

  fullUser: Observable<UserCustomization>;
  user: Observable<UserCustomization>;

  getFullUser(user: UserCustomization): Observable<UserCustomization> {
    if (!this.fullUser) {
      this.fullUser = this.getUser(user.email).pipe(catchError(this.handleErrorGetUser(user)),
        mergeMap(user1 => this.searchHistoryService.fillSearchHistory(user1)), publishReplay(1), refCount()
      );
    }
    return this.fullUser;
  }

  clearCacheFullUser() {
    this.fullUser = null;
  }

  private handleErrorGetUser(user: UserCustomization) {
    return (error: any): Observable<UserCustomization> => {
      if (error.status === 404) {
        this.user = null;//empehe la mise en cache de la 404
        return this.postUser(user);
      } else {
        return Observable.throw(new Error(error.status));
      }
    };
  }

  putUser(user: User): Observable<UserCustomization> {
    console.log('put');
    return this._http.put<UserCustomization>(this.userUrl, user)
      .pipe(
        catchError(this.handleError)
      );
  }

  private handleError(err: HttpErrorResponse) {

    console.log(err);
    return observableThrowError(err);
  }


}
