import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {UserCustomization} from './user-customization';
import {Observable, of} from 'rxjs';
import {UserCustomizationService} from './user-customization.service';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<UserCustomization> {
  constructor(private userCustomizationService: UserCustomizationService, private userService: UserService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<UserCustomization> {
    if (!this.userService.isAuthorize()) {
      return of(new UserCustomization('', '', '', false, true));
    }
    return this.userCustomizationService.getFullUser(this.userService.userCustomization);
  }

}
