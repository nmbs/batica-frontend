import {ICompany} from '../company/company';
import {User} from './user';

export class UserCustomization implements User {
  constructor(public nom: string, public  prenom: string, public email: string, public tutorial: boolean, public newUser: boolean) {
  }

  guid: string;
  history: ICompany[];
}
