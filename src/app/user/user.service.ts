import {KeycloakService} from 'keycloak-angular';
import {Injectable} from '@angular/core';
import {UserCustomization} from './user-customization';


@Injectable()
export class UserService {

  constructor(private keycloakService: KeycloakService) {

  }

  public personFirstName: string = this.keycloakService.getKeycloakInstance().profile.firstName;
  public personLastName: string = this.keycloakService.getKeycloakInstance().profile.lastName;
  public personEmail: string = this.keycloakService.getKeycloakInstance().profile.email;
  public personToken: string = this.keycloakService.getKeycloakInstance().token;
  public roles: string[] = this.keycloakService.getKeycloakInstance().realmAccess.roles;
  public userCustomization = new UserCustomization(this.personLastName, this.personFirstName, this.personEmail, false, true);

  public isAuthorize(): boolean {
    let res = false;
    this.roles.forEach(role => {
      if (role == 'admin' || role == 'manager' || role == 'user') {
        res = true;
      }
    });
    return res;
  }


}
