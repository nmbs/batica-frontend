import {inject, TestBed} from '@angular/core/testing';

import {UserResolverService} from './user-resolver.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {UserService} from './user.service';
import {UserCustomizationService} from './user-customization.service';
import {TestUtils} from '../test-utils/test-utils';
import {AppConfig} from '../app.config';
import {KeycloakService} from 'keycloak-angular';
import {CompanyService} from '../company/company.service';
import {MockAppConfig} from '../app.config.mock';

describe('UserResolverService', () => {
  beforeEach(() => {
    const appConfigSpy = new MockAppConfig();
    const keycloakServiceSpy = new TestUtils().getKeycloakServiceSpy();
    TestBed.configureTestingModule({
      providers: [UserService, CompanyService, UserCustomizationService,
        UserResolverService, {provide: AppConfig, useValue: appConfigSpy,}, {provide: KeycloakService, useValue: keycloakServiceSpy}],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([UserResolverService], (service: UserResolverService) => {
    expect(service).toBeTruthy();
  }));
});
