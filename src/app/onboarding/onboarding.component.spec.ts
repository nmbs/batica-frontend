import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {OnboardingComponent} from './onboarding.component';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {AppConfig} from '../app.config';
import {MockAppConfig} from '../app.config.mock';
import {UserService} from '../user/user.service';
import {KeycloakService} from 'keycloak-angular';
import {TestUtils} from '../test-utils/test-utils';
import {SearchHistoryService} from '../history/search-history.service';
import {CompanyService} from '../company/company.service';

describe('OnboardingComponent', () => {
  let component: OnboardingComponent;
  let fixture: ComponentFixture<OnboardingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OnboardingComponent],
      providers: [{provide: AppConfig, useValue: new MockAppConfig()}, UserService, SearchHistoryService, CompanyService, {
        provide: KeycloakService,
        useValue: new TestUtils().getKeycloakServiceSpy()
      }],
      imports: [HttpClientTestingModule]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnboardingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
