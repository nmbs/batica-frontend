import {Component, EventEmitter, Injectable, OnInit, Output} from '@angular/core';
import {UserService} from '../user/user.service';
import {UserCustomizationService} from '../user/user-customization.service';

@Component({
  selector: 'app-onboarding',
  templateUrl: './onboarding.component.html',
  styleUrls: ['./onboarding.component.scss']
})
@Injectable()
export class OnboardingComponent implements OnInit {

  public displayNone;
  public isClickHide;
  @Output() OnHideOnBoarding = new EventEmitter<boolean>();

  constructor(private userCustomizationService: UserCustomizationService,
              private userService: UserService) {
  }

  ngOnInit() {

  }

  public onClickDecouvrirBatica() {
    // On change les classes du composant et des composants parents
    this.isClickHide = true;
    this.OnHideOnBoarding.next(true);
    this.userService.userCustomization.newUser = false;
    this.userCustomizationService.putUser(this.userService.userCustomization).subscribe(data => {
    });

    setTimeout(() => {
      this.displayNone = true;
    }, 2000);

  }

}
