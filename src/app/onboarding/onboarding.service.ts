import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OnboardingService {
  onboarding = new Subject<boolean>();
  onboarding$ = this.onboarding.asObservable();

  public launchOnboarding() {
    this.onboarding.next(true);
  }
}
