import {KeycloakConfig} from 'keycloak-angular';

export interface Config {
  backendUrl: string;
  userUrl: string;
  dashboardUrl: string;
  searchHistoryUrl: string;
  customizationUrl: string;
  keycloakConfig: KeycloakConfig;
}
