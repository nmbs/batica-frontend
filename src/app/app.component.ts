import {Component} from '@angular/core';
import {KeycloakService} from 'keycloak-angular';
import {ActivatedRoute, Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {UserCustomizationService} from './user/user-customization.service';
import {Subscription} from 'rxjs';
import {UserService} from './user/user.service';
import {OnboardingComponent} from './onboarding/onboarding.component';
import {OnboardingService} from './onboarding/onboarding.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  pageTitle: string = 'Batica';
  loading: boolean = true;
  private subscription: Subscription;
  public isOnboardingAffiche;
  public displayNone = false;

  constructor(private route: ActivatedRoute,
              private keycloakService: KeycloakService,
              private router: Router,
              private userCustomizationService: UserCustomizationService,
              private userService: UserService,
              private onboardingComponent: OnboardingComponent, private onboardingService: OnboardingService
  ) {
    router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });

  }

  public ngOnInit(): void {
    //Faire l'appel à custom api
    this.isOnboardingAffiche = false;
    this.onboardingService.onboarding$.subscribe(value=>{
      console.log("5/5")
      this.isOnboardingAffiche = true;
    })


  }

  checkRouterEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.loading = true;
    }

    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loading = false;
    }
  }

  public clickButton(value) {
    if (value) {
      this.isOnboardingAffiche = false;
      setTimeout(function () {
        this.displayNone = true;
      }, 20);
    }
  }

  ngAfterContentInit() {

  }

  ngOnDestroy() {
    this.onboardingComponent.OnHideOnBoarding.unsubscribe();
  }


}
