import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {NgbModule, NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

import {AppComponent} from './app.component';
import {SearchBoxComponent} from './search-box/search-box.component';
import {ListComponent} from './list/list.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {CompanyComponent} from './company/company.component';
import {CompanyService} from './company/company.service';
import {HeaderComponent} from './header/header.component';
import {NotificationComponent} from './notification/notification.component';
import {AvatarComponent} from './avatar/avatar.component';
import {SettingsComponent} from './settings/settings.component';
import {TutorialComponent} from './tutorial/tutorial.component';
import {FilterComponent} from './list/filter/filter.component';
import {SearchHistoryComponent} from './tutorial/search-history/search-history.component';
import {FilterListPipe} from './_shared/filter-list.pipe';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ClickOutsideDirective} from './_shared/click-outside.directive';
import {AutocompleteFilterPipe} from './_shared/autocomplete-filter.pipe';
import {KeycloakAngularModule, KeycloakService} from 'keycloak-angular';
import {UserService} from './user/user.service';
import {initializer} from './app-init';
import {AppConfig} from './app.config';
import {UserCustomizationService} from './user/user-customization.service';
import {SearchHistoryService} from './history/search-history.service';
import {PendingCreationComponent} from './pending-creation/pending-creation.component';
import {FavorisPortefeuilleService} from './favoris-portefeuille/favoris-portefeuille.service';
import {DashboardComponent, NgbdModalContent} from './dashboard/dashboard.component';
import {OnboardingComponent} from './onboarding/onboarding.component';
import {BreadcrumbComponent} from './breadcrumb/breadcrumb.component';
import {NewsComponent} from './tutorial/news/news.component';
import {NewsDashboardComponent} from './dashboard/news/news.component';
import {registerLocaleData} from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import localeFrExtra from '@angular/common/locales/extra/fr';
import {UnitePipe} from './dashboard/unite.pipe';
import {KPIPipe} from './dashboard/kpi.pipe';
import {LocalisationComponent} from './dashboard/localisation/localisation.component';
import {OrderFilter} from './_shared/filter-order';
import { KpiCComponent } from './dashboard/kpi-c/kpi-c.component';
import { KpiSComponent } from './dashboard/kpi-s/kpi-s.component';
import { KpiKComponent } from './dashboard/kpi-k/kpi-k.component';
import { KpiAComponent } from './dashboard/kpi-a/kpi-a.component';
import { KpiBComponent } from './dashboard/kpi-b/kpi-b.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchBoxComponent,
    ListComponent,
    CompanyComponent,
    HeaderComponent,
    NotificationComponent,
    AvatarComponent,
    SettingsComponent,
    TutorialComponent,
    FilterComponent,
    SearchHistoryComponent,
    FilterListPipe,
    ClickOutsideDirective,
    AutocompleteFilterPipe,
    PendingCreationComponent,
    OnboardingComponent,
    DashboardComponent,
    BreadcrumbComponent,
    NewsComponent,
    NewsDashboardComponent,
    UnitePipe,
    KPIPipe,
    LocalisationComponent,
    OrderFilter,
    KpiCComponent,
    KpiSComponent,
    KpiKComponent,
    KpiAComponent,
    KpiBComponent,
    NgbdModalContent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatInputModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MatTableModule,
    BrowserAnimationsModule,
    KeycloakAngularModule,
    NgbModule.forRoot()
  ],
  entryComponents: [
    NgbdModalContent
  ],
  providers: [
    HttpClient,
    UserService,
    AppConfig,
    CompanyService,
    UserCustomizationService,
    SearchHistoryService,
    TutorialComponent,
    FavorisPortefeuilleService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializer,
      multi: true,
      deps: [AppConfig, KeycloakService]
    },
    OnboardingComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

registerLocaleData(localeFr, 'fr-FR', localeFrExtra);
