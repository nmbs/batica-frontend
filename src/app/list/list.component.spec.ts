import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {CompanyService} from '../company/company.service';
import {ListComponent} from './list.component';
import {Observable, of} from 'rxjs';
import {ICompany} from '../company/company';
import {FilterComponent} from './filter/filter.component';
import {SearchBoxComponent} from '../search-box/search-box.component';
import {FormsModule} from '@angular/forms';
import {ActivatedRoute, convertToParamMap} from '@angular/router';
import {RouterTestingModule} from '@angular/router/testing';

export class ActivatedRouteMock {
  public paramMap = of(convertToParamMap({
    searchTerm: '1',
  }));
}

class MockCompanyService {
  getCompaniesBySiren(searchTerm: string): Observable<ICompany[]> {

    return Observable.create((observer) => {
      const resp = [{
        'siret': '12345678910',
        'waldec': 'lenumerodewaldec1',
        'raisonSociale': 'Renault',
        'dirigeant': 'pierre jean',
        'adresse': {
          'voie': '16 rue dunois',
          'codePostal': '75006',
          'commune': 'paris'
        },
        'eirl': true,
        'activiteAuxiliaire': false,
        'siege': true,
        'status': 'KO'
      }];
      observer.next(resp);
      observer.complete();
    });
  }

}

describe('ListComponent', () => {
  let component: ListComponent;
  let fixture: ComponentFixture<ListComponent>;
  let activatedRoute: ActivatedRouteMock;
  let routerStub;
  beforeEach(async(() => {
    routerStub = {
      navigate: jasmine.createSpy('navigate')
    };
    activatedRoute = new ActivatedRouteMock();
    // activatedRoute.testParamMap = { searchTerm: '1' };
    TestBed.configureTestingModule({
      imports: [FormsModule, RouterTestingModule],
      providers: [
        {provide: CompanyService, useClass: MockCompanyService},
        {provide: ActivatedRoute, useValue: activatedRoute}
      ],
      declarations: [ListComponent, SearchBoxComponent, FilterComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
