import {Component, ElementRef, HostListener, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Event, NavigationCancel, NavigationEnd, NavigationError, NavigationStart, Router} from '@angular/router';
import {MatPaginator, MatTableDataSource} from '@angular/material';
import {ICompany} from '../company/company';
import {SearchHistoryService} from '../history/search-history.service';
import {FavorisRequest} from '../favoris-portefeuille/favoris-request';
import {UserCustomizationService} from '../user/user-customization.service';
import {FavorisPortefeuilleService} from '../favoris-portefeuille/favoris-portefeuille.service';
import {UserCustomization} from '../user/user-customization';
import {PortefeuilleRequest} from '../favoris-portefeuille/portefeuille-request';
import {forkJoin} from 'rxjs';
import {UserService} from '../user/user.service';
import {User} from '../user/user';
import {Favoris} from '../favoris-portefeuille/favoris';
import {BreadcrumbService} from '../breadcrumb/breadcrumb.service';
import {Crumb} from '../breadcrumb/breadcrumb';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnDestroy {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('divAjoutPortefeuille', {read: ElementRef}) divAjoutPortefeuille: ElementRef;
  @ViewChild('divPortefeuille', {read: ElementRef}) divPortefeuille: ElementRef;
  @ViewChild('divFavori', {read: ElementRef}) divFavori: ElementRef;
  //@ViewChild("divTroisPetitsPoints", {read: ElementRef}) divTroisPetitsPoints: ElementRef;
  divTroisPetitsPointsClicked;
  isCurrentEntreprisePresentInPortefeuille;
  isCurrentEntreprisePresentInFavori;
  errorMessage: string;
  searchTerm: string;
  isTableResult: boolean = false;
  dataSource = new MatTableDataSource([]);
  private sub: any;
  index = 0;
  nbResults = 20;
  pageSizeOptions = [20, 50, 100];
  pageNumber = 0;
  numbers = [];
  isScrollBarVisible: boolean = false;
  loading = false;
  userCusto: UserCustomization;
  private entreprise: ICompany;
  clicked = '';
  user: User;
  isUserNew: boolean;
  listFavoris: Array<Favoris> = [];
  listePortefeuille: Array<Favoris> = [];
  isFilterFix = false;
  ngIfAfficherRetirerPortefeuille = false;
  ngIfAfficherRetirerFav = false;
  ngStyle: Object;

  message = {
    searchSirenSiret: '',
    typoEIRL: false,
    typoSiege: false,
    typoActivite: false,
    typoActif: false,
    typoFerme: false,
    searchVille: '',
    searchCP: ''
  };

  displayedColumns = ['raisonSociale', 'adresse', 'dirigeant', 'siege', 'status', 'info-complementaires', 'portefeuille'];
  public list = [];
  public filteredList = [];
  ngStyleDivTroisPetitsPoints: Object = {};

  constructor(private route: ActivatedRoute,
              private userCustomizationService: UserCustomizationService,
              private searchHistoryService: SearchHistoryService,
              private favorisPortefeuilleService: FavorisPortefeuilleService,
              private userService: UserService,
              private router: Router,
              private breadcrumbService: BreadcrumbService) {
    this.list = [{}];
    this.filteredList = [];
    router.events.subscribe((routerEvent: Event) => {
      this.checkRouterEvent(routerEvent);
    });
  }

  filterOff(): boolean {
    return JSON.stringify(this.list) == JSON.stringify(this.filteredList);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.searchTerm = params['searchTerm'];
    });

    this.route.data
      .subscribe((data: { companyResolver: ICompany[] }) => {
        this.list = data.companyResolver;
        this.filteredList = data.companyResolver;
        if (this.list.length > 0) {
          this.filteredList = this.list;
          this.dataSource = new MatTableDataSource(this.filteredList);
          this.getNumberOfPages();
          //Récupération de l'utilisateur, ses favoris et ses portefeuilles
          this.userCustomizationService.getFullUser(this.userService.userCustomization).subscribe(user => {
            this.userService.userCustomization = user;
            let mesFavoris = this.favorisPortefeuilleService.getFavoris(this.userService.userCustomization.guid);
            let mesPortefeuilles = this.favorisPortefeuilleService.getPortefeuille(this.userService.userCustomization.guid);
            forkJoin([mesFavoris, mesPortefeuilles]).subscribe(results => {

              let entreprises = this.list;
              let favoris = results[0];
              let portefeuilles = results[1];
              entreprises.forEach(entreprise => {
                favoris.forEach(favori => {
                  if ((entreprise.siret && entreprise.siret != '' && entreprise.siret == favori.siret) || (entreprise.waldec && entreprise.waldec != '' && entreprise.waldec == favori.waldec)) {
                    entreprise.isFavori = favori.guid;
                  }
                });
                portefeuilles.forEach(portefeuille => {
                  if ((entreprise.siret && entreprise.siret != '' && entreprise.siret == portefeuille.siret) || (entreprise.waldec && entreprise.waldec != '' && entreprise.waldec == portefeuille.waldec)) {
                    entreprise.isPortefeuille = portefeuille.guid;
                  }
                });

              });
              this.filteredList = this.list;
              this.dataSource = new MatTableDataSource(this.filteredList);
              this.getNumberOfPages();
            });
          });
        }
      });

    this.sub = this.route.params.subscribe(params => {

      this.searchTerm = params['searchTerm'];

      let crumb: Crumb = {
        path: '/list/',
        param: this.searchTerm,
        label: 'Recherche',
        url: '/list/' + this.searchTerm
      };

      this.breadcrumbService.refreshBreadcrumb(crumb);
    });

  }

  ngAfterContentChecked() {
    if (this.paginator && !this.dataSource.paginator) {
      this.dataSource.paginator = this.paginator;
      this.refreshTable(this.dataSource.paginator.pageSize);
    }
  }

  getNumberOfPages(): void {
    this.pageNumber = Math.ceil(this.filteredList.length / this.nbResults);
    this.numbers = Array(this.pageNumber).fill(null, 0).map((x, i) => i);
  }

  getNumberOfPagesWithIndex(j): void {
    this.pageNumber = Math.ceil(this.filteredList.length / j);
    this.numbers = Array(this.pageNumber).fill(null, 0).map((x, i) => i);
  }

  refreshTable(i) {
    this.dataSource.paginator.pageSize = i;
    this.dataSource.paginator.pageIndex = 0;
    this.getNumberOfPagesWithIndex(i);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator(this.filteredList.length);
  }

  clickPage(i) {
    this.dataSource.paginator.pageIndex = i;
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator(this.filteredList.length);
  }

  clickNext() {
    if (this.dataSource.paginator.pageIndex < this.numbers.length - 1) {
      this.dataSource.paginator.pageIndex++;
      this.dataSource._updateChangeSubscription();
      this.dataSource._updatePaginator(this.filteredList.length);
    }
  }

  clickTop() {
    document.body.scrollTop = 0; //Pour Safari
    document.documentElement.scrollTop = 0; //Pour les autres
  }

  @HostListener('window:scroll', ['$event'])
  onScroll(event) {

    let number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

    if (number > 0) {
      this.isScrollBarVisible = true;
    }

    else {
      this.isScrollBarVisible = false;
    }

    if (number > 326 && window.innerHeight > 600) {
      this.isFilterFix = true;
    }
    else {
      this.isFilterFix = false;
    }

  }

  listFilter(filters): void {
    this.filteredList = this.list.filter(item => {
      var includeItem = true;
      for (var prop in filters) {
        // skip loop if the property is from prototype
        if (!filters.hasOwnProperty(prop)) continue;

        let value = filters[prop];

        if (typeof value === 'boolean' && value === true) {
          includeItem = item[prop] === true;
          if (!includeItem) break;
        }

        else if (typeof value === 'string' && value !== '') {
          if (prop === 'siret') {
            if (item[prop] != undefined) {
              includeItem = (item[prop].includes(value) == true);
              if (includeItem == false) break;
            }
            else {
              includeItem = (item['waldec'].includes(value) == true);
              if (includeItem == false) break;
            }
          }
          else if ((prop === 'codePostal' || prop === 'commune') && item.adresse[prop] != undefined) {
            includeItem = (item.adresse[prop].includes(value) == true);
            if (includeItem == false) break;
          }
          else {
            includeItem = false;
            if (includeItem == false) break;
          }
        }
      }

      return includeItem;
    });
    this.refreshTable(this.dataSource.paginator.pageSize);
  }

  search(entreprise: ICompany): void {
    this.router.navigate(['entreprises', entreprise.waldec ? entreprise.waldec : entreprise.siret]);
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  filterList(filters) {
    this.listFilter(filters);
    this.dataSource = new MatTableDataSource(this.filteredList);
    this.getNumberOfPages();
  }

  showFilterBloc(): boolean {
    return this.list.length > 0;
  }

  addCompanyHistorySearch(comapny: ICompany): void {

    this.searchHistoryService.postCompanySearchHistory(comapny).subscribe(data => {
    }, er => {
      console.log('error', er);
    });
    this.userCustomizationService.clearCacheFullUser();
    this.search(comapny);
  }

  hideDivFavoris(event) {
    if (!event.target.attributes || event.target.attributes['class'].nodeValue.indexOf('cdk-column-portefeuille mat-column-portefeuille') == -1 && event.target.attributes['class'].nodeValue.indexOf('picto-ajout') == -1) {
      try {
        this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille-gris.svg\')';

        this.ngStyle['display'] = 'none';
        if (this.divTroisPetitsPointsClicked) {
          this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille-gris.svg\')';
        }
      } catch (ex) {
      }
    }
  }

  onClickTroisPetitsPoints(event, item) {

    var ngIfAfficherDiv = false;
    if (this.entreprise) {
      this.entreprise.isSelected = false;
    }

    var left = String(event.clientX - 220);
    var top = String(event.clientY + document.body.scrollTop + document.documentElement.scrollTop);

    this.ngStyle = {'left': left + 'px', 'top': top + 'px', 'display': 'flex'};

    if (this.divTroisPetitsPointsClicked) {
      this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille-gris.svg\')';
    }

    this.entreprise = item;
    this.entreprise.isSelected = true;
    if (this.entreprise.isFavori != undefined) {
      this.isCurrentEntreprisePresentInFavori = true;
    }
    else {
      this.isCurrentEntreprisePresentInFavori = false;
    }
    if (this.entreprise.isPortefeuille != undefined) {
      this.isCurrentEntreprisePresentInPortefeuille = true;
    }
    else {
      this.isCurrentEntreprisePresentInPortefeuille = false;
    }
    if (!event.originalTarget)
      return;
    if (event.originalTarget.firstChild != null) {
      this.divTroisPetitsPointsClicked = event.originalTarget.firstChild;
    }
    else {
      this.divTroisPetitsPointsClicked = event.originalTarget;
    }
    this.ngStyleDivTroisPetitsPoints['background-image'] = 'url(\'../../assets/icones/portefeuille.svg\')';
  }

  addFavoris(): void {

    let favorisRequest: FavorisRequest;

    favorisRequest = {
      'waldec': this.entreprise.waldec == undefined || this.entreprise.waldec == null ? '' : this.entreprise.waldec,
      'siret': this.entreprise.siret == undefined || this.entreprise.siret == null ? '' : this.entreprise.siret,
      'idUtilisateur': this.userService.userCustomization.guid
    };

    if (this.entreprise.isFavori == undefined) {
      this.favorisPortefeuilleService.postFavoris(favorisRequest).subscribe(data => {
        this.filteredList.forEach((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == favorisRequest.siret) || entreprise.waldec && (entreprise.waldec == favorisRequest.waldec)) {
            entreprise.isFavori = data.guid;
            this.filteredList[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.filteredList);
        });
      });
    }
    else {
      this.favorisPortefeuilleService.deleteFavori(this.entreprise.isFavori).subscribe(data => {
        this.filteredList.forEach((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == favorisRequest.siret) || entreprise.waldec && (entreprise.waldec == favorisRequest.waldec)) {
            entreprise.isFavori = undefined;
            this.filteredList[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.filteredList);
        });

      });
    }
  }

  addPortefeuilles(): void {
    let portefeuilleRequest: PortefeuilleRequest;
    portefeuilleRequest = {
      'waldec': this.entreprise.waldec,
      'siret': this.entreprise.siret,
      'idUtilisateur': this.userService.userCustomization.guid
    };
    if (this.entreprise.isPortefeuille == undefined) {
      this.favorisPortefeuilleService.postPortefeuille(portefeuilleRequest).subscribe(data => {
        this.filteredList.forEach((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == portefeuilleRequest.siret) || entreprise.waldec && (entreprise.waldec == portefeuilleRequest.waldec)) {
            entreprise.isPortefeuille = data.guid;
            this.filteredList[i] = entreprise;
          }
          this.dataSource = new MatTableDataSource(this.filteredList);
        });
      });
    }
    else {
      this.favorisPortefeuilleService.deletePortefeuille(this.entreprise.isPortefeuille).subscribe(data => {
        this.filteredList.forEach((entreprise, i) => {
          if (entreprise.siret && (entreprise.siret == portefeuilleRequest.siret) || entreprise.waldec && (entreprise.waldec == portefeuilleRequest.waldec)) {

            entreprise.isPortefeuille = undefined;
            this.filteredList[i] = entreprise;

          }
          this.dataSource = new MatTableDataSource(this.filteredList);
        });
      });
    }
  }

  checkRouterEvent(routerEvent: Event): void {
    if (routerEvent instanceof NavigationStart) {
      this.loading = true;
    }

    if (routerEvent instanceof NavigationEnd ||
      routerEvent instanceof NavigationCancel ||
      routerEvent instanceof NavigationError) {
      this.loading = false;
    }
  }
}
