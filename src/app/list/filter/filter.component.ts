import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {IFilter} from './filter';
import {ICompany} from '../../company/company';

@Component({
  selector: 'filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  filters: IFilter;
  initialFilters: IFilter = {
    siret: '',
    eirl: false,
    siege: false,
    actif: false,
    ferme: false,
    commune: '',
    codePostal: ''
  };

  @Output() filterChanged = new EventEmitter<IFilter>();

  @Input() list;

  @Input() filteredList;

  @Input() isFilterFix;

  showDropDownSiret = false;

  showDropDownCodePostal = false;

  showDropDownCommune = false;

  listCommunes = Array();

  ngOnInit() {
    this.filters = Object.assign({}, this.initialFilters);
    this.initFilter();
  }

  selectValueFromDropDown(field: string, value: ICompany): void {

    if (field === 'siret') {
      if (value.siret && value.siret != '') {
        this.filters[field] = value.siret;
      }
      else {
        this.filters[field] = value.waldec;
      }
      this.toggleDropDownSiret();
    }
    else if (field === 'commune') {
      this.filters[field] = value.adresse.commune;
      this.toggleDropDownCommune();
    }
    else {
      this.filters[field] = value.adresse.codePostal;
      this.toggleDropDownCodePostal();
    }

  }

  initFilter() {
    this.list.forEach(entreprise => {
      if (!this.listCommunes.includes(entreprise.adresse['commune'])) {
        this.listCommunes.push(entreprise.adresse['commune']);
      }
    });
  }

  getSearchValue(field: string) {
    return this.filters[field];
  }

  toggleDropDownSiret(): void {

    this.showDropDownSiret = !this.showDropDownSiret;
    this.showDropDownCodePostal = false;
    this.showDropDownCommune = false;
  }

  getSearchValueCodePostal() {
    return this.filters.siret;
  }

  toggleDropDownCodePostal(): void {

    this.showDropDownCodePostal = !this.showDropDownCodePostal;
    this.showDropDownSiret = false;
    this.showDropDownCommune = false;
  }

  getSearchValueCommune() {
    return this.filters.siret;
  }

  toggleDropDownCommune(): void {

    this.showDropDownCommune = !this.showDropDownCommune;
    this.showDropDownCodePostal = false;
    this.showDropDownSiret = false;
  }

  sendFilters() {
    this.filterChanged.emit(this.filters);
  }

  eraseFilter() {
    this.filters = Object.assign({}, this.initialFilters);
    this.sendFilters();
  }

  showFilters(): boolean {
    return JSON.stringify(this.list) == JSON.stringify(this.filteredList);
  }

}
