export interface IFilter {
  eirl: boolean,
  siege: boolean,
  actif: boolean,
  ferme: boolean,
  siret: string,
  commune: string,
  codePostal: string,
}
