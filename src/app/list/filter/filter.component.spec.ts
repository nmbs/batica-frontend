import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {FilterComponent} from './filter.component';
import {FormsModule} from '@angular/forms';
import {AutocompleteFilterPipe} from '../../_shared/autocomplete-filter.pipe';
import {OrderFilter} from '../../_shared/filter-order';

describe('FilterComponent', () => {
  let component: FilterComponent;
  let fixture: ComponentFixture<FilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule],
      declarations: [FilterComponent, AutocompleteFilterPipe, OrderFilter],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterComponent);
    component = fixture.componentInstance;
    component.list = [];
    component.filteredList = [];

    fixture.detectChanges();
  });

  it('should expect', () => {
    expect(component).toBeTruthy();
    component.eraseFilter();
    expect(JSON.stringify(this.filters) == JSON.stringify(this.initialFilters));
  });
});
