#!/bin/bash
set -e

CFG_DIR=/usr/share/nginx/html/assets/configuration

# Replace env variables in the configuration file
cp $CFG_DIR/config.json $CFG_DIR/config.json.origin
envsubst < $CFG_DIR/config.json.origin > $CFG_DIR/config.json

exec "$@"
